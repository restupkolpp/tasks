import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


public class MoySkladRu {

    private void authorization(WebDriver driver, Wait<WebDriver> wait){

        wait.until(d -> driver.findElement(By.xpath("//*[@name='j_username']")).isDisplayed());

        // Вводим логин
        driver.findElement(By.xpath("//*[@name='j_username']")).click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@name='j_username']")), driver.switchTo().activeElement());
        driver.findElement(By.xpath("//*[@name='j_username']")).sendKeys("admin@anton2003");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        // Вводим пароль
        driver.findElement(By.xpath("//*[@name='j_password']")).click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@name='j_password']")), driver.switchTo().activeElement());
        driver.findElement(By.xpath("//*[@name='j_password']")).sendKeys("28jcbhbc");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        // Нажимаем кнопку войти
        driver.findElement(By.xpath("//*[@class='b-button']")).click();
    }

    public String waitUntilDownloadCompleted(WebDriver driver) throws InterruptedException {
        /**
         * Метод, который возвращает имя последнего скачанного файла в браузере.
         *
         * @param driver Экземпляр класса WebDriver.
         * @return fileName Возвращает имя скачанного файла.
         */

        String mainWindow = driver.getWindowHandle();

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.open()");

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        driver.get("chrome://downloads");

        JavascriptExecutor js1 = (JavascriptExecutor) driver;

        Long percentage = (long) 0;
        while (percentage != 100) {
            try {
                percentage = (Long) js1.executeScript("return document.querySelector('downloads-manager').shadowRoot.querySelector('#downloadsList downloads-item').shadowRoot.querySelector('#progress').value");

            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }

        String fileName = (String) js1.executeScript("return document.querySelector('downloads-manager').shadowRoot.querySelector('#downloadsList downloads-item').shadowRoot.querySelector('div#content #file-link').text");
        String sourceURL = (String) js1.executeScript("return document.querySelector('downloads-manager').shadowRoot.querySelector('#downloadsList downloads-item').shadowRoot.querySelector('div#content #file-link').href");
        String donwloadedAt = (String) js1.executeScript("return document.querySelector('downloads-manager').shadowRoot.querySelector('#downloadsList downloads-item').shadowRoot.querySelector('div.is-active.focus-row-active #file-icon-wrapper img').src");

        System.out.println("Download deatils");
        System.out.println("File Name :-" + fileName);
        System.out.println("Donwloaded path :- " + donwloadedAt);
        System.out.println("Downloaded from url :- " + sourceURL);

        System.out.println(fileName);
        System.out.println(sourceURL);

        driver.close();

        driver.switchTo().window(mainWindow);
        return fileName;
    }

    @Test
    public void createNewItem(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Антон\\Desktop\\Папки\\Лабы\\4 курс 1 семестр\\Тестирование\\Курсовая\\Course\\drivers\\chromedriver-win64\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(15));

        driver.get("https://online.moysklad.ru/app/#homepage");

        authorization(driver, wait);

        // Ждем пока прогрузится следующий элемент и кликаем на "Товары"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/table/tbody/tr/td[1]/div/table/tbody/tr/td[8]/table/tbody/tr[1]/td/img")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/table/tbody/tr/td[1]/div/table/tbody/tr/td[8]/table/tbody/tr[1]/td/img")).click();

        // Ждем пока прогрузится следующий элемент и кликаем на "Товары и услуги"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/div[2]/div/span[1]/a")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/div[2]/div/span[1]/a")).click();

        // Ждем пока прогрузится следующий элемент и кликаем на "+ Товар"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[3]/table/tbody/tr/td/div/div/table/tbody/tr[1]/td/div/div[2]/table/tbody/tr/td[2]/div/table/tbody/tr/td[2]/span")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[3]/table/tbody/tr/td/div/div/table/tbody/tr[1]/td/div/div[2]/table/tbody/tr/td[2]/div/table/tbody/tr/td[2]/span")).click();

        // Ждем пока прогрузится следующий элемент и кликаем на "Наименование товара"
        wait.until(d -> driver.findElement(By.xpath("//*[@class='gwt-TextBox field b-validation-field']")).isDisplayed());
        driver.findElement(By.xpath("//*[@class='gwt-TextBox field b-validation-field']")).click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@class='gwt-TextBox field b-validation-field']")), driver.switchTo().activeElement());
        driver.findElement(By.xpath("//*[@class='gwt-TextBox field b-validation-field']")).sendKeys("Холодильник");

        // Вводим минимальную цену
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[2]/div[1]/table/tbody/tr/td/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div/div[1]/table/tbody/tr[1]/td[2]/div/table/tbody/tr/td[1]/div/input")).click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[2]/div[1]/table/tbody/tr/td/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div/div[1]/table/tbody/tr[1]/td[2]/div/table/tbody/tr/td[1]/div/input")), driver.switchTo().activeElement());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[2]/div[1]/table/tbody/tr/td/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div/div[1]/table/tbody/tr[1]/td[2]/div/table/tbody/tr/td[1]/div/input")).sendKeys("3000");

        // Вводим закупочную цену
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[2]/div[1]/table/tbody/tr/td/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div/div[1]/table/tbody/tr[2]/td[2]/div/table/tbody/tr/td[1]/div/input")).click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[2]/div[1]/table/tbody/tr/td/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div/div[1]/table/tbody/tr[2]/td[2]/div/table/tbody/tr/td[1]/div/input")), driver.switchTo().activeElement());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[2]/div[1]/table/tbody/tr/td/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div/div[1]/table/tbody/tr[2]/td[2]/div/table/tbody/tr/td[1]/div/input")).sendKeys("2800");

        // Вводим цену продажи
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[2]/div[1]/table/tbody/tr/td/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div/div[1]/table/tbody/tr[4]/td[2]/div/table/tbody/tr/td[1]/div/input")).click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[2]/div[1]/table/tbody/tr/td/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div/div[1]/table/tbody/tr[4]/td[2]/div/table/tbody/tr/td[1]/div/input")), driver.switchTo().activeElement());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[2]/div[1]/table/tbody/tr/td/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/div/div[1]/table/tbody/tr[4]/td[2]/div/table/tbody/tr/td[1]/div/input")).sendKeys("3300");

        // Нажимаем кнопку сохранить
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr[2]/td/div/div[2]/div[1]/table/tbody/tr/td[2]/span")).click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        // Нажимаем кнопку закрыть
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr[2]/td/div/div[2]/div[2]/table/tbody/tr/td[2]/span")).click();

        //Проверяем
        wait.until(d -> driver.findElement(By.xpath("//*[@title='Холодильник']")).isDisplayed());
        Assert.assertEquals("Холодильник", driver.findElement(By.xpath("//*[@title='Холодильник']")).getText());

        System.out.println("Создан новый товар: " + driver.findElement(By.xpath("//*[@title='Холодильник']")).getText());

        driver.quit();

    }

    @Test
    public void deleteItem(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Антон\\Desktop\\Папки\\Лабы\\4 курс 1 семестр\\Тестирование\\Курсовая\\Course\\drivers\\chromedriver-win64\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(15));

        driver.get("https://online.moysklad.ru/app/#homepage");

        authorization(driver, wait);

        // Ждем пока прогрузится следующий элемент и кликаем на "Товары"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/table/tbody/tr/td[1]/div/table/tbody/tr/td[8]/table/tbody/tr[1]/td/img")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/table/tbody/tr/td[1]/div/table/tbody/tr/td[8]/table/tbody/tr[1]/td/img")).click();

        // Ждем пока прогрузится следующий элемент и кликаем на "Товары и услуги"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/div[2]/div/span[1]/a")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/div[2]/div/span[1]/a")).click();

        // Ждем пока прогрузится следующий элемент и кликаем на товар
        wait.until(d -> driver.findElement(By.xpath("//*[@title='Холодильник']")).isDisplayed());
        driver.findElement(By.xpath("//*[@title='Холодильник']")).click();

        // Ждем пока прогрузится страница с информацией о товаре и кликаем на многоточие
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr[2]/td/div/div[2]/div[8]/table/tbody/tr/td[3]")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr[2]/td/div/div[2]/div[8]/table/tbody/tr/td[3]")).click();

        // Нажимаем на кнопку удалить
        wait.until(d -> driver.findElement(By.xpath("/html/body/div[16]/div/div/table/tbody/tr[3]")).isDisplayed());
        driver.findElement(By.xpath("/html/body/div[16]/div/div/table/tbody/tr[3]")).click();

        // Подтверждаем удаление
        driver.findElement(By.xpath("/html/body/div[16]/div/table/tbody/tr/td[1]/a")).click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        //Проверяем
        try {
            Assert.assertEquals("Холодильник", driver.findElement(By.xpath("//*[@title='Холодильник']")).getText());
        } catch (Exception e){
            System.out.println("Товар был удален");
        }

        driver.quit();

    }

    @Test
    public void createOrder(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Антон\\Desktop\\Папки\\Лабы\\4 курс 1 семестр\\Тестирование\\Курсовая\\Course\\drivers\\chromedriver-win64\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(15));

        driver.get("https://online.moysklad.ru/app/#homepage");

        authorization(driver, wait);

        // Ждем пока прогрузится следующий элемент и кликаем на "Продажи"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/table/tbody/tr/td[1]/div/table/tbody/tr/td[6]/table/tbody/tr[1]/td/img")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/table/tbody/tr/td[1]/div/table/tbody/tr/td[6]/table/tbody/tr[1]/td/img")).click();

        // Ждем пока прогрузится следующий элемент и кликаем на "Заказы покупателей"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/div[2]/div/span[1]/a")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/div[2]/div/span[1]/a")).click();

        // Ждем пока прогрузится следующий элемент и кликаем на кнопку "+ Заказ"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[3]/table/tbody/tr/td/div/div/table/tbody/tr[1]/td/div/div[2]/table/tbody/tr/td[2]/div/table/tbody/tr/td[1]")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[3]/table/tbody/tr/td/div/div/table/tbody/tr[1]/td/div/div[2]/table/tbody/tr/td[2]/div/table/tbody/tr/td[1]")).click();

        // Ждем пока прогрузится страница с информацией о товаре и кликаем на Контрагента
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/div/div[1]/div[1]/div[5]/div[1]/div[6]/div/div/div/div[1]")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/div/div[1]/div[1]/div[5]/div[1]/div[6]/div/div/div/div[1]")).click();

        // Выбираем покупателя
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/div/div[1]/div[1]/div[5]/div[1]/div[6]/div/div[1]/div/div[3]/div[1]/div[2]")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/div/div[1]/div[1]/div[5]/div[1]/div[6]/div/div[1]/div/div[3]/div[1]/div[2]")).click();

        // Вводим номер заказа
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/div/div[1]/div[1]/div[4]/div[1]/div")).click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/div/div[1]/div[1]/div[4]/div[1]/div")), driver.switchTo().activeElement());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/div/div[1]/div[1]/div[4]/div[1]/div")).sendKeys("2");

        // Выбираем товар из справочника
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/div/div[1]/div[2]/div/div[2]/div[2]/div[1]/button")).click();

        // Добавляем количество товара
        wait.until(d -> driver.findElement(By.xpath("/html/body/div[17]/div/div/div/div/div[2]/div[2]/div/table/tbody/tr/td[2]/div/table[1]/tbody[1]/tr[2]/td[5]/div/div/span[2]"))).isDisplayed();
        for (int i = 0; i < 2; i++){
            driver.findElement(By.xpath("/html/body/div[17]/div/div/div/div/div[2]/div[2]/div/table/tbody/tr/td[2]/div/table[1]/tbody[1]/tr[2]/td[5]/div/div/span[2]")).click();
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
        }

        //Нажимаем кнопку Добавить
        driver.findElement(By.xpath("/html/body/div[17]/div/div/div/div/div[2]/table/tbody/tr/td[1]/div/table/tbody/tr/td[2]")).click();

        //Нажимаем кнопку Сохранить
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/div/div[1]/div[1]/div[2]/div/div/div[1]/table/tbody/tr/td[2]")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/div/div[1]/div[1]/div[2]/div/div/div[1]/table/tbody/tr/td[2]")).click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        // Нажимаем кнопку закрыть
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/div/div[1]/div[1]/div[2]/div/div/div[2]/table/tbody/tr/td[2]")).click();

        //Проверяем
        Assert.assertEquals("2", driver.findElement(By.xpath("//*[@id=\"DocumentTableCustomerOrder\"]/tbody[1]/tr[1]/td[2]/div/a")).getText());
        System.out.println("Заказ был создан");

        driver.quit();

    }

    @Test
    public void filterCheck(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Антон\\Desktop\\Папки\\Лабы\\4 курс 1 семестр\\Тестирование\\Курсовая\\Course\\drivers\\chromedriver-win64\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(15));

        driver.get("https://online.moysklad.ru/app/#homepage");

        authorization(driver, wait);

        // Ждем пока прогрузится следующий элемент и кликаем на "Товары"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/table/tbody/tr/td[1]/div/table/tbody/tr/td[8]/table/tbody/tr[1]/td/img")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/table/tbody/tr/td[1]/div/table/tbody/tr/td[8]/table/tbody/tr[1]/td/img")).click();

        // Ждем пока прогрузится следующий элемент и кликаем на "Инвентаризации"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/div[2]/div/span[4]/a")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/div[2]/div/span[4]/a")).click();

        // Ждем пока прогрузится следующий элемент и кликаем на "Фильтр"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[3]/table/tbody/tr/td/div/div/table/tbody/tr[1]/td/div/div[2]/table/tbody/tr/td[3]/table/tbody/tr/td[1]/div/table/tbody/tr/td[2]")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[3]/table/tbody/tr/td/div/div/table/tbody/tr[1]/td/div/div[2]/table/tbody/tr/td[3]/table/tbody/tr/td[1]/div/table/tbody/tr/td[2]")).click();

        // Ждем пока прогрузится следующий элемент и кликаем на "Товар или группа"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[3]/table/tbody/tr/td/div/div/table/tbody/tr[2]/td/div/div/div/div[2]/div[3]/div/div[2]/div")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[3]/table/tbody/tr/td/div/div/table/tbody/tr[2]/td/div/div/div/div[2]/div[3]/div/div[2]/div")).click();

        // Ждем пока прогрузится следующий элемент и кликаем на наименование товара
        wait.until(d -> driver.findElement(By.xpath("/html/body/div[19]/div/div/div/div/div[2]/div[2]/div/table/tbody/tr/td[2]/div/table[1]/tbody[1]/tr[1]/td[3]")).isDisplayed());
        driver.findElement(By.xpath("/html/body/div[19]/div/div/div/div/div[2]/div[2]/div/table/tbody/tr/td[2]/div/table[1]/tbody[1]/tr[1]/td[3]")).click();

        // Кликаем на кнопку "Выбрать".
        driver.findElement(By.xpath("/html/body/div[19]/div/div/div/div/div[2]/table/tbody/tr/td[1]/div/table/tbody/tr/td[2]")).click();

        // Ждем пока прогрузится следующий элемент и кликаем на кнопку "Найти"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[3]/table/tbody/tr/td/div/div/table/tbody/tr[2]/td/div/div/div/table/tbody/tr/td[1]/div")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[3]/table/tbody/tr/td/div/div/table/tbody/tr[2]/td/div/div/div/table/tbody/tr/td[1]/div")).click();

        //Проверяем
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"DocumentTableAbstractInventory\"]/tbody[1]/tr[1]/td[4]/div/a")).isDisplayed());
        Assert.assertEquals("00003", driver.findElement(By.xpath("//*[@id=\"DocumentTableAbstractInventory\"]/tbody[1]/tr[1]/td[4]/div/a")).getText());

        System.out.println("Фильтр применился");

        driver.quit();

    }

    @Test
    public void orderExport() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Антон\\Desktop\\Папки\\Лабы\\4 курс 1 семестр\\Тестирование\\Курсовая\\Course\\drivers\\chromedriver-win64\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(15));

        driver.get("https://online.moysklad.ru/app/#homepage");

        authorization(driver, wait);

        // Ждем пока прогрузится следующий элемент и кликаем на "Продажи"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/table/tbody/tr/td[1]/div/table/tbody/tr/td[6]/table/tbody/tr[1]/td/img")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/table/tbody/tr/td[1]/div/table/tbody/tr/td[6]/table/tbody/tr[1]/td/img")).click();

        // Ждем пока прогрузится следующий элемент и кликаем на "Заказы покупателей"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/div[2]/div/span[1]/a")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[4]/td/div/div[2]/div/span[1]/a")).click();

        // Ждем пока прогрузится следующий элемент и кликаем на кнопку "Печать"
        wait.until(d -> driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[3]/table/tbody/tr/td/div/div/table/tbody/tr[1]/td/div/div[2]/table/tbody/tr/td[4]/table/tbody/tr/td[5]/div/table/tbody/tr/td[2]")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"site\"]/table/tbody/tr[5]/td/table/tbody/tr/td[3]/table/tbody/tr/td/div/div/table/tbody/tr[1]/td/div/div[2]/table/tbody/tr/td[4]/table/tbody/tr/td[5]/div/table/tbody/tr/td[2]")).click();

        // Ждем пока прогрузится страница и кликаем на Список заказов
        wait.until(d -> driver.findElement(By.xpath("/html/body/div[18]/div/div/table/tbody/tr[1]")).isDisplayed());
        driver.findElement(By.xpath("/html/body/div[18]/div/div/table/tbody/tr[1]")).click();

        // Выбираем формат
        WebElement selectElement = driver.findElement(By.xpath("/html/body/div[20]/div/div/div/div/div[2]/div[2]/div/div/table/tbody/tr[1]/td/select"));
        Select select = new Select(selectElement);
        select.selectByVisibleText("Скачать в формате PDF");

        //Нажимаем кнопку Да
        driver.findElement(By.xpath("/html/body/div[19]/div/div/div/div/div[2]/div[3]/table/tbody/tr/td[1]/div/table/tbody/tr/td[2]/span")).click();

        try {
            System.out.println(waitUntilDownloadCompleted(driver));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        driver.quit();

    }

}
