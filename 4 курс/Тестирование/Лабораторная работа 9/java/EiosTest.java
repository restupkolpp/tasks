import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


public class EiosTest {

    @Test
    public void eiosTest(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Антон\\Desktop\\Папки\\Лабы\\4 курс 1 семестр\\Тестирование\\Лабораторная 9\\lab9\\drivers\\chromedriver-win64\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.get("https://eios.kemsu.ru/a/eios");

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));

        driver.findElement(By.xpath("//*[@name='username']")).click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@name='username']")), driver.switchTo().activeElement());
        driver.findElement(By.xpath("//*[@name='username']")).sendKeys("stud71717");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));

        driver.findElement(By.xpath("//*[@name='password']")).click();
        Assert.assertEquals(driver.findElement(By.xpath("//*[@name='password']")), driver.switchTo().activeElement());
        driver.findElement(By.xpath("//*[@name='password']")).sendKeys("28jcbhbc");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));

        driver.findElement(By.xpath("//*[@class='css-h0m9oy efn4aem0']")).click();

        Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(d -> driver.findElement(By.xpath("//a[@href='/a/eios/profile' and @class='css-10pdxt6 efn4aem0']")).isDisplayed());

        driver.findElement(By.xpath("//a[@href='/a/eios/profile' and @class='css-10pdxt6 efn4aem0']")).click();

        wait.until(d -> driver.findElement(By.xpath("//div[label[text()='ФИО']]/input[@readonly]")).isDisplayed());

        Assert.assertEquals("Аношин Антон Павлович", driver.findElement(By.xpath("//div[label[text()='ФИО']]/input[@readonly]")).getDomAttribute("value"));

        driver.quit();

    }

}
