import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;


public class MainClass {

    @Test
    public void FirstTest(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Антон\\Desktop\\Папки\\Лабы\\4 курс 1 семестр\\Тестирование\\Лабораторная 9\\lab9\\drivers\\chromedriver-win64\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.get("https://habr.com/ru/articles/");

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.findElement(By.xpath("//*[@class='tm-svg-img tm-header-user-menu__icon tm-header-user-menu__icon_search tm-header-user-menu__icon_dark']")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@class='tm-search__input tm-input-text-decorated__input']")), driver.switchTo().activeElement());
        driver.findElement(By.xpath("//*[@class='tm-search__input tm-input-text-decorated__input']")).sendKeys("Selenium WebDriver");

        driver.findElement(By.xpath("//*[@class='tm-svg-img tm-svg-icon']")).click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        driver.findElement(By.linkText("Что такое Selenium WebDriver?")).click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

//        Assert.assertEquals("28 сентября 2012 в 17:14", driver.findElement(By.xpath("//*[@title='2012-10-01, 16:40']")).getText());

        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        driver.manage().window().setSize(new Dimension(1280, 1025));

        driver.findElement(By.xpath("//a[@href='/ru/articles/' and @class='footer-menu__item-link']")).click();

        driver.quit();

    }

}
