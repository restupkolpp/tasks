def is_prime(n):
    for divisor in range(2, int(n ** 0.5)):
        if n % divisor == 0:
            return False
    return True


def my_test_is_prime():
    test_cases = (
        ('not a number', None),
        (-1, None),
        (0, None),
        (1, None),
        (2, True),
        (3, True),
        (6, False),
        (7, True),
        (9, False),
        (16, False),
    )
    for in_n, correct_answer in test_cases:
        try:
            answer = is_prime(in_n)
        except TypeError as E:
            if type(in_n) == int:
                print(f"Ошибка! Не удалось вычислить is_prime({in_n}). Ошибка: {E}")
                return False
        except ValueError as E:
            if in_n > 1:
                print(f"Ошибка! Не удалось вычислить is_prime({in_n}). Ошибка: {E}")
                return False
        else:
            if correct_answer != answer:
                print(f'Ошибка! reverse({in_n}) равно {correct_answer} вместо "{answer}"')
                return False
    return True


my_test_is_prime()
