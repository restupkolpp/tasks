def is_correct_mobile_phone_number_ru(s):
    count = 0
    if s == '':
        return False
    if s[0] == '8' or s[0:2] == '+7':
        s = s.replace('+', '', 1)
        for letter in s[1:]:
            if letter.isdigit():
                count += 1
        if count == 10:
            if '(' in s or ')' in s:
                if s.count('(') == s.count(')') == 1:
                    stroka = s.replace(' ', '')
                    stroka = stroka.replace('-', '')
                    if stroka[1] != '(' and stroka[5] != ')':
                        return False
                else:
                    return False
            s = s.replace('(', '')
            s = s.replace(')', '')
            s = s.replace('-', ' ')
            indexes = [1, 5, 9, 12]
            stroka = s.replace(' ', '')
            stroka = stroka[0] + ' ' + stroka[1:4] + ' ' + stroka[4:7] + ' ' + stroka[7:9] + ' ' + stroka[9:]

            if stroka != s:
                return False
            return True
        else:
            return False
    else:
        return False


def my_test_is_correct_mobile_phone_number_ru():
    test_cases = (
        ('', False),
        ('+7' + 'a' * 10, False),
        ('+89991112233', False),
        ('+79991112233', True),
        ('89991112233', True),
        ('8-800-111-11-11', True),
        ('+7-800-111-11-11', True),
        ('8 (999) 123-45-67', True),
        ('8 (999 123-45-67', False),
        ('8 )999( 123-45-67', False),  # can't just cut off parentheses
        ('8 (999) (123)-45-67', False),  # even paired parentheses may be incorrect
    )
    for in_s, correct_answer in test_cases:
        answer = is_correct_mobile_phone_number_ru(in_s)
        if answer != correct_answer:
            return False
    return True


print('YES') if my_test_is_correct_mobile_phone_number_ru() else print('NO')
