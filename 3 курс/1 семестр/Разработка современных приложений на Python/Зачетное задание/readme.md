# Movies Database

A database with movies. With this program, you can add movies you like and change information about them.

## There are two classes in the program:

- MovieDatabaseApp
- Dialog

The first one is needed in order to run the main application in which you can add and delete entries
The second opens a dialog box where you can update records in the database

## Tech

The application uses a number of libraries to work properly:

- numpy
- PyQt5
- PyQt5-Qt5
- PyQt5-sip

## About methods

Now we will describe the main methods by which the application works

- create_connection (responsible for connecting to the database)
- create_table (responsible for creating the database)
- load_data (responsible for adding information to the database and displaying it on the widget)
- update_base (responsible for updating the database and displaying these updates on the widget)
- delete_movie (responsible for removing a certain movie from the database)
- add_movie (responsible for adding the movie to the database)
