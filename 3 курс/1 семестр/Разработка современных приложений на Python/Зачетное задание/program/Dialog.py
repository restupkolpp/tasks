from PyQt5.QtWidgets import QDialog, QLineEdit, QPushButton, QGridLayout, QLabel, QMessageBox


class Dialog(QDialog):
    def __init__(self, data=None, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Добавить запись")

        # Создаем поля ввода для каждого столбца таблицы
        self.id_line = QLineEdit()
        self.title_line = QLineEdit(data[1] if data else '')
        self.year_line = QLineEdit()
        self.genres = QLineEdit()

        # Создаем кнопки "Сохранить" и "Отмена"
        self.add_button = QPushButton("Сохранить")
        self.cancel_button = QPushButton("Отмена")
        self.add_button.clicked.connect(self.accept)
        self.cancel_button.clicked.connect(self.reject)

        # Создаем сетку для размещения элементов управления
        layout = QGridLayout()
        layout.addWidget(QLabel("ID"), 0, 0)
        layout.addWidget(self.id_line, 0, 1)
        layout.addWidget(QLabel("Название фильма:"), 1, 0)
        layout.addWidget(self.title_line, 1, 1)
        layout.addWidget(QLabel("Год:"), 2, 0)
        layout.addWidget(self.year_line, 2, 1)
        layout.addWidget(QLabel("Жанр:"), 3, 0)
        layout.addWidget(self.genres, 3, 1)
        layout.addWidget(self.add_button, 4, 0)
        layout.addWidget(self.cancel_button, 4, 1)

        self.setLayout(layout)

    def accept(self):
        # Проверяем корректность года
        try:
            if self.id_line.text() == '':
                raise ValueError("Введите id!")
            else:
                l_id = int(self.id_line.text())
        except ValueError as e:
            QMessageBox.warning(self, "Ошибка", str(e))
            return
        super().accept()

    def get_data(self):
        l_id = self.id_line.text()
        title = self.title_line.text()
        year = self.year_line.text()
        genre = self.genres.text()
        return l_id, title, year, genre
