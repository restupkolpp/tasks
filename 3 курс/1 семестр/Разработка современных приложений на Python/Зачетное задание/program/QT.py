import datetime
import sys
import sqlite3
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidgetItem, QMessageBox

from form import Ui_MainWindow
from Dialog import Dialog


class MovieDatabaseApp(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.create_connection()
        self.create_table()
        self.initUi()

    def initUi(self):
        self.setWindowTitle('База данных фильмов')
        self.add_button.clicked.connect(self.add_movie)
        self.movie_list.setColumnCount(4)
        self.movie_list.setHorizontalHeaderLabels(['ID', 'Название', 'Год', 'Жанр'])

        self.movie_list.setColumnWidth(0, 50)
        self.movie_list.setColumnWidth(1, 250)
        self.movie_list.setColumnWidth(2, 100)
        self.movie_list.setColumnWidth(3, 250)

        self.update_button.clicked.connect(self.update_base)

        self.delete_check.stateChanged.connect(self.showDeleteMenu)
        self.delete_film.setDisabled(True)

        self.load_data()
        self.delete_film.clicked.connect(self.delete_movie)

    def showDeleteMenu(self):
        if self.delete_check.isChecked():
            self.delete_film.setDisabled(False)
        else:
            self.delete_film.setDisabled(True)

    def create_connection(self):
        self.conn = sqlite3.connect('../movies.db')
        self.cur = self.conn.cursor()

    def create_table(self):
        self.cur.execute('''CREATE TABLE IF NOT EXISTS movies (
                            id INTEGER PRIMARY KEY,
                            title TEXT,
                            year INTEGER,
                            genre TEXT)''')
        self.conn.commit()

    def load_data(self):
        self.cur.execute('''SELECT * FROM movies''')
        movies = self.cur.fetchall()
        self.movie_list.setRowCount(0)
        for row_number, row_data in enumerate(movies):
            self.movie_list.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                self.movie_list.setItem(row_number, column_number, QTableWidgetItem(str(data)))

    def update_base(self):
        dialog = Dialog(None, self)
        if dialog.exec_():
            l_id, l_title, l_year, l_genre = dialog.get_data()

            if int(l_year) > datetime.datetime.now().year:
                QMessageBox.warning(self, "Ошибка", "Неверно введен год")
                return

            if l_title == '' and l_year == '' and l_genre == '':
                QMessageBox.information(self, 'Title', 'Вы не ввели данные для обновления')

            elif l_title != '' and l_year == '' and l_genre == '':
                self.cur.execute("Update movies set title=? where id=?", (l_title, l_id))
                self.conn.commit()
                self.load_data()

            elif l_title == '' and l_year != '' and l_genre == '':
                self.cur.execute("Update movies set year=? where id=?", (l_year, l_id))
                self.conn.commit()
                self.load_data()

            elif l_title == '' and l_year == '' and l_genre != '':
                self.cur.execute("Update movies set genre=? where id=?", (l_genre, l_id))
                self.conn.commit()
                self.load_data()

            elif l_title != '' and l_year != '' and l_genre == '':
                self.cur.execute("Update movies set title=?, year=? where id=?", (l_title, l_year, l_id))
                self.conn.commit()
                self.load_data()

            elif l_title != '' and l_year == '' and l_genre != '':
                self.cur.execute("Update movies set title=?, genre=? where id=?", (l_title, l_genre, l_id))
                self.conn.commit()
                self.load_data()

            elif l_title == '' and l_year != '' and l_genre != '':
                self.cur.execute("Update movies set year=?, genre=? where id=?", (l_year, l_genre, l_id))
                self.conn.commit()
                self.load_data()

            elif l_title != '' and l_year != '' and l_genre != '':
                self.cur.execute("Update movies set title=?, year=?, genre=? where id=?", (l_title, l_year, l_genre, l_id))
                self.conn.commit()
                self.load_data()

    def delete_movie(self):
        l_id = self.id_input.text()
        if l_id and int(l_id) >= 0:
            try:
                l_id = int(l_id)
                self.cur.execute(f'''DELETE from movies where id = {l_id}''')
                self.conn.commit()
                self.id_input.clear()
                self.load_data()
            except ValueError:
                QMessageBox.warning(self, 'Ошибка', 'Пожалуйста, введите корректный id')
        else:
            QMessageBox.warning(self, 'Ошибка', 'Пожалуйста, заполните поле ввода')

    def add_movie(self):
        title = self.film_name.text()
        year = self.year_input.text()
        genre = self.genre_input.text()

        if title and year and genre:
            try:
                year = int(year)
                self.cur.execute('''INSERT INTO movies (title, year, genre) 
                                                VALUES (?, ?, ?)''',
                                 (title, year, genre))
                self.conn.commit()
                self.film_name.clear()
                self.year_input.clear()
                self.genre_input.clear()
                self.load_data()
            except ValueError:
                QMessageBox.warning(self, 'Ошибка', 'Пожалуйста, введите корректный год')
        else:
            QMessageBox.warning(self, 'Ошибка', 'Пожалуйста, заполните все поля')


def except_hook(cls, exception, traceback):
    sys.__excepthook__(cls, exception, traceback)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MovieDatabaseApp()
    window.show()
    sys.excepthook = except_hook
    sys.exit(app.exec_())
