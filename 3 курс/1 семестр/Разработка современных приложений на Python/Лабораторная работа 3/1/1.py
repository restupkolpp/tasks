lang = ['йцукенгшщзхъ', 'фывапролджэё', 'ячсмитьбю', 'qwertyuiop', 'asdfghjkl', 'zxcvbnm']
nums = set('1234567890')
sla = set('йцукенгшщзхъфывапролджэячсмитьбюqwertyuiopasdfghjklzxcvbnm')
sua = set('ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮQWERTYUIOPASDFGHJKLZXCVBNM')


def invalidPasswordCheck(password):
    spsw = set(password)
    if not (spsw & sla and spsw & sua and spsw & nums and len(password) > 8):
        return 'error'
    for c in range(len(password) - 2):
        psw_ = password[c:c + 3].lower()
        for i in lang:
            if psw_ in i:
                return 'error'
    return 'ok'


passw = input("Введите пароль:")
print(invalidPasswordCheck(passw))
