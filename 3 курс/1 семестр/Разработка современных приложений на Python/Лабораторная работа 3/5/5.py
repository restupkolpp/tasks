class PasswordError(Exception):
    pass


class FormatError(PasswordError):
    pass


class CodeError(PasswordError):
    pass


class DigitError(PasswordError):
    pass


class OperatorError(PasswordError):
    pass


def checkPhoneNumber(number):
    number = number.replace(" ", "").replace("\t", "")
    if (number.count('(') == 1 and number.count(')') == 1) and \
            (number.index('(') < number.index(')') or ('(' not in number and ')' not in number)):
        start_index = number.index("(")
        end_index = number.index(")")
        number = number[:start_index] + number[start_index + 1:end_index] + number[end_index + 1:]
    else:
        raise FormatError

    if "--" not in number and "-" in number:
        number = number[0] + number[1:-1].replace("-", "") + number[-1]
    else:
        raise FormatError

    if number.startswith("+7"):
        number = "+7" + number[2:]
    elif number.startswith("8"):
        number = "+7" + number[1:]
    else:
        raise CodeError

    if len(number[1:]) == 11 and number[1:].isdigit():
        return number
    else:
        raise DigitError


try:
    num = input("Введите номер телефона: ")
    print(checkPhoneNumber(num))
except Exception as error:
    n = error.__class__.__name__
    if n == 'FormatError':
        print("Неверный формат")
    elif n == 'DigitError':
        print("Неверное количество цифр")
    elif n == 'CodeError':
        print("Неверный код страны")
    else:
        print("Не определяется оператор сотовой связи")
