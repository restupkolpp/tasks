class PasswordError(Exception):
    pass


class LengthError(PasswordError):
    pass


class LetterError(PasswordError):
    pass


class DigitError(PasswordError):
    pass


class SequenceError(PasswordError):
    pass


def invalidPasswordCheck(password):
    lang = ['йцукенгшщзхъ', 'фывапролджэё', 'ячсмитьбю', 'qwertyuiop', 'asdfghjkl', 'zxcvbnm']
    nums = set('1234567890')
    sla = set('йцукенгшщзхъфывапролджэячсмитьбюqwertyuiopasdfghjklzxcvbnm')
    sua = set('ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮQWERTYUIOPASDFGHJKLZXCVBNM')
    spsw = set(password)
    try:
        if not spsw & sla:
            raise LetterError
        if not spsw & sua:
            raise LetterError
        if not spsw & nums:
            raise DigitError
        if len(password) < 9:
            raise LengthError
        for c in range(len(password) - 2):
            psw_ = password[c:c + 3].lower()
            for i in lang:
                if psw_ in i:
                    raise SequenceError
        return 'ok'
    except PasswordError as pe:
        raise pe


try:
    passw = input("Введите пароль:")
    print(invalidPasswordCheck(passw))
except Exception as error:
    print(error.__class__.__name__)
