lang = ['йцукенгшщзхъ', 'фывапролджэё', 'ячсмитьбю', 'qwertyuiop', 'asdfghjkl', 'zxcvbnm']
nums = set('1234567890')
sla = set('йцукенгшщзхъфывапролджэячсмитьбюqwertyuiopasdfghjklzxcvbnm')
sua = set('ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮQWERTYUIOPASDFGHJKLZXCVBNM')


def invalidPasswordCheck(password):
    spsw = set(password)

    assert len(password) > 8
    assert spsw & sla
    assert spsw & nums
    assert spsw & sua
    for c in range(len(password) - 2):
        psw_ = password[c:c + 3].lower()
        for i in lang:
            assert psw_ not in i
    return 'ok'


try:
    passw = input("Введите пароль: ")
    print(invalidPasswordCheck(passw))
except AssertionError:
    print("error")
