import sys

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow


class MyWidget(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('1.ui', self)
        self.setFixedSize(800, 600)
        self.pushButton.clicked.connect(self.run)

    def run(self):
        self.label_2.setText(f'Цвет флага: {self.buttonGroup.checkedButton().text()}, '
                             f'{self.buttonGroup_2.checkedButton().text()}, '
                             f'{self.buttonGroup_3.checkedButton().text()}')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyWidget()
    ex.show()
    sys.exit(app.exec_())
