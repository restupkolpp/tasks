import difflib as df
import sys

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QStatusBar


class MyWidget(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('5.ui', self)
        self.statusBar = QStatusBar()
        self.setStatusBar(self.statusBar)
        self.compareButton.clicked.connect(self.compare)

    def compare(self):
        str1 = self.textEdit_1.toPlainText()
        str2 = self.textEdit_2.toPlainText()

        y = df.SequenceMatcher(None, str1.lower(), str2.lower()).ratio() * 100
        str = self.threshold.text().replace(",", ".")
        if self.threshold.text() and str != "0.00":
            if float(str) >= round(y, 2):
                self.statusBar.showMessage(f'Текст похож на {round(y, 2)}%')
                self.statusBar.setStyleSheet('background-color: green')
            else:
                self.statusBar.showMessage(f'Текст похож на {round(y, 2)}%')
                self.statusBar.setStyleSheet('background-color: red')
        else:
            self.statusBar.showMessage("Введите порог сравнения!")
            self.statusBar.setStyleSheet('background-color: red;')


def except_hook(cls, exception, traceback):
    sys.__excepthook__(cls, exception, traceback)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyWidget()
    ex.show()
    sys.excepthook = except_hook
    sys.exit(app.exec_())
