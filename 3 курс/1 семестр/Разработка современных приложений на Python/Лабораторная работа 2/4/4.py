import random
import sys

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox


class MyWidget(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('4.ui', self)

        self.setButton.clicked.connect(self.setCount)
        self.takeButton.clicked.connect(self.takeStone)
        self.newGameButton.clicked.connect(self.newGame)

    def setCount(self):
        self.stoneCount.setText(self.countLine.text())

    def takeStone(self):
        if (int(self.stoneToTake.text())) > 3 or (int(self.stoneToTake.text()) < 0):
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setWindowTitle("Уведомление")
            msg.setText("Вы не можете взять больше трех камней за раз")
            msg.exec_()
        else:
            if int(self.stoneCount.text()) > 0:
                self.playerTake()
                self.compTake()

    def playerTake(self):
        self.listWidget.addItem(f'Вы взяли - {self.stoneToTake.text()}')
        self.stoneCount.setText(str(int(self.stoneCount.text()) - int(self.stoneToTake.text())))
        if int(self.stoneCount.text()) == 0:
            self.winLabel.setText("Победа за вами!")

    def compTake(self):
        if not int(self.stoneCount.text()) == 0:
            if int(self.stoneCount.text()) % 4 == 0:
                n = random.randrange(1, 4)
                self.listWidget.addItem(f'Компьютер взял - {n}')
                self.stoneCount.setText(str(int(self.stoneCount.text()) - n))
            else:
                self.listWidget.addItem(f'Компьютер взял - {int(self.stoneCount.text()) % 4}')
                self.stoneCount.setText(str(int(self.stoneCount.text()) - int(self.stoneCount.text()) % 4))
                if int(self.stoneCount.text()) == 0:
                    self.winLabel.setText("Победа за компьютером!")

    def newGame(self):
        self.countLine.setText("")
        self.stoneCount.setText("")
        self.stoneToTake.setText("")
        self.winLabel.setText("")
        self.listWidget.clear()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyWidget()
    ex.show()
    sys.exit(app.exec_())
