import sys

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow


class MyWidget(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('3.ui', self)
        self.pushButton.clicked.connect(self.run)

    def run(self):
        name = self.lineEdit.text()
        number = self.lineEdit_2.text()
        self.listWidget.addItem(f'{name} - {number}')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyWidget()
    ex.show()
    sys.exit(app.exec_())
