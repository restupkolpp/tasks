import sys

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow


class MyWidget(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('2.ui', self)
        self.pushButton.clicked.connect(self.run)

    def run(self):
        date = self.calendarWidget.selectedDate().toPyDate()
        plan = self.lineEdit.text()
        dt = self.timeEdit.time().toPyTime()
        self.listWidget.addItem(f'{date} {dt} - {plan}')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyWidget()
    ex.show()
    sys.exit(app.exec_())
