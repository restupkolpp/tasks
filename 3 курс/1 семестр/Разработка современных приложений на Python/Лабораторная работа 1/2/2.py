import sys

from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QLineEdit


class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setGeometry(500, 500, 500, 500)
        self.setWindowTitle('Второе задание')

        self.name_input = QLineEdit(self)
        self.name_input.move(200, 100)

        self.btn = QPushButton('Расчитать', self)
        self.btn.resize(self.btn.sizeHint())
        self.btn.move(210, 150)
        self.btn.clicked.connect(self.evalFunc)

        self.name_input_2 = QLineEdit(self)
        self.name_input_2.move(200, 200)

    def evalFunc(self):
        self.name_input_2.setText(str(eval(self.name_input.text())))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    ex.show()
    sys.exit(app.exec())
