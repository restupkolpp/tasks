import sys

from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QLineEdit


class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setGeometry(500, 500, 500, 500)
        self.setWindowTitle('Первое задание')

        self.name_input = QLineEdit(self)
        self.name_input.move(200, 100)

        self.btn = QPushButton('Кнопка', self)
        self.btn.resize(self.btn.sizeHint())
        self.btn.move(210, 150)
        self.btn.clicked.connect(self.moveText)

        self.name_input_2 = QLineEdit(self)
        self.name_input_2.move(200, 200)

    def moveText(self):
        if self.name_input_2.text() == '':
            self.name_input_2.setText(self.name_input.text())
            self.name_input.setText('')
        else:
            self.name_input.setText(self.name_input_2.text())
            self.name_input_2.setText('')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    ex.show()
    sys.exit(app.exec())
