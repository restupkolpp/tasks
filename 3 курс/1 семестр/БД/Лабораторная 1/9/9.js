let student = {
    surename: "Аношин",
    name: "Алексей",
    mid_name: "Павлович",
    birth_year: "2007",
    class: "11",
    favorite_disciplines: ["Математика", "Английский язык", "Физика"],

    learning_period: 11,

    Period_check: function(){
        var period = this.learning_period;
        if(period != 9 && period != 11) return 2;
        if(this.class < 1 || this.class > 11) return 3;
        if(this.class == period) return 1;
        else return 0;
    },

    Change_class: function(){
        var periodCheck = this.Period_check();
        if(periodCheck == 0)
        {
            this.class += 1; 
            return 0;
        }
        else if(periodCheck == 1)
        {
            alert("Максимальный класс уже достигнут!"); 
            return 1;
        }
        else if (periodCheck == 2) 
        {
            alert("Error in Learning Period!"); 
            return 2;
        }
        else if(periodCheck == 3) 
        {
            alert("Error in current class!"); 
            return 3;
        }
    },

    location_reg: {
        country: "Россия",
        city: "Новокузнецк",
        street: "Звездова",
        build_number: "24",
        flat_number: "46",
        location_info: function(){
            return `Страна: ${this.country} <br><br> Город: ${this.city} <br><br> Улица: ${this.street} <br><br> Дом №: ${this.build_number} <br><br> Квартира: ${this.flat_number}`;
        }
    },
    current_location: {
        country: "Россия",
        city: "Кемерово",
        street: "Васильева",
        build_number: "20",
        flat_number: "310",
        current_location_info: function(){
            return `Страна: ${this.country} <br><br> Город: ${this.city} <br><br> Улица: ${this.street} <br><br> Дом №: ${this.build_number} <br><br> Квартира: ${this.flat_number}`;
        }
    },
    show: function()
    {
        label = document.getElementById('lb');
        let str = ``;
        str += `Фамилия: ${this.surename} <br><br>`;
        str += `Имя: ${this.name} <br><br>`;
        str += `Отчество: ${this.mid_name} <br><br>`;
        str += `Год рождения: ${this.birth_year} <br><br>`;
        str += `Класс: ${this.class} <br><br>`;

        let favorite_disciplines_text = "Любимые предметы: ";
        this.favorite_disciplines.forEach((item) => {
            favorite_disciplines_text += `${item}, `;
        });

        str += `${favorite_disciplines_text}`;
        str += `<h4>Адрес регистрации:</h4> ${this.location_reg.location_info()} <br><br>`;
        str += `<h4>Место проживания:</h4> ${this.current_location.current_location_info()} <br><br>`;

        if(this.Period_check() == 1){str += `Максимальный класс уже достигнут! <br><br>`}

        label.innerHTML = str;
    },

    /*setBirthYear: function()
    {
        let label = document.getElementById('lb');
        let x = document.getElementById('in').value;
        var now = new Date();
        now = now.getFullYear();
        if((now - x) > 200 || x >= now){
            label.innerHTML = "Ошибка, год неверный";
        }else{
            label.innerHTML = " ";
            this.birth_year = x;
        }
    }*/
}

student.show();
