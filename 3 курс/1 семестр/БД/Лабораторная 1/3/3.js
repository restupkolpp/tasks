let label = document.getElementById('lb')

function fact(x)
{
    if(x == 0) 
    {
        return 1;
    }
    return x * fact(x - 1);
}

function middle(x)
{
    let sum = 0;
    for (let i = 1; i <= x; i++)
    {
        sum += i;
    }
    return sum / x;
}

function main()
{
    let number = document.getElementById('in').value;
    if (document.getElementById('in').value % 2 == 0)
    {
        label.innerHTML = `Факториал: ${fact(number)}`;
    }
    else
    {
        label.innerHTML = `Среднее арифметическое: ${middle(number)}`;
    }
}
