label = document.getElementById('lb');

let students = [
    ["Anoshin", "Anton", 3, 1],
    ["Ustinov", "Alexey", 3, 1],
    ["Ivanov", "Ivan", 2, 1],
    ["Plisko", "Ivan", 2, 2],
    ["Loboda", "Maxim", 4, 2],
    ["Astanin", "Anatoly", 4, 3]
];

function main()
{
    console.clear();
    let str = ``
    dir_id = document.getElementById('in').value;
    for (let i = 0; i <= 5; i++)
    {
        if (dir_id == students[i][3])
        {
            str += `| First name: ${students[i][0]} | Last name: ${students[i][1]} | Course: ${students[i][2]} | Direction id: ${students[i][3]} | <br><br>`;
        }
        else
        {
            console.log(`| First name: ${students[i][0]} | Last name: ${students[i][1]} | Course: ${students[i][2]} | Direction id: ${students[i][3]}|`)
        }
    }
    label.innerHTML = str;
}
