let student = {
    surename: "Аношин",
    name: "Алексей",
    mid_name: "Павлович",
    birth_year: "2007",
    class: "10",
    fv_dscpls: ["Математика", "Английский язык", "Физика"],
    location_reg: {
        country: "Россия",
        city: "Новокузнецк",
        street: "Звездова",
        build_number: "24",
        flat_number: "46"
    },
    current_location: {
        country: "Россия",
        city: "Кемерово",
        street: "Васильева",
        build_number: "20",
        flat_number: "310"
    }
}