let student = {
    surename: "Аношин",
    name: "Алексей",
    mid_name: "Павлович",
    birth_year: "2007",
    class: "10",
    favorite_disciplines: ["Математика", "Английский язык", "Физика"],
    location_reg: {
        country: "Россия",
        city: "Новокузнецк",
        street: "Звездова",
        build_number: "24",
        flat_number: "46",
        location_info: function(){
            return `Страна: ${this.country} <br><br> Город: ${this.city} <br><br> Улица: ${this.street} <br><br> Дом №: ${this.build_number} <br><br> Квартира: ${this.flat_number}`;
        }
    },
    current_location: {
        country: "Россия",
        city: "Кемерово",
        street: "Васильева",
        build_number: "20",
        flat_number: "310",
        current_location_info: function(){
            return `Страна: ${this.country} <br><br> Город: ${this.city} <br><br> Улица: ${this.street} <br><br> Дом №: ${this.build_number} <br><br> Квартира: ${this.flat_number}`;
        }
    },
    show: function()
    {
        label = document.getElementById('lb');
        let str = ``;
        str += `Фамилия: ${this.surename} <br><br>`;
        str += `Имя: ${this.name} <br><br>`;
        str += `Отчество: ${this.mid_name} <br><br>`;
        str += `Год рождения: ${this.birth_year} <br><br>`;
        str += `Класс: ${this.class} <br><br>`;

        let favorite_disciplines_text = "Любимые предметы: ";
        this.favorite_disciplines.forEach((item) => {
            favorite_disciplines_text += `${item}, `;
        });

        str += `${favorite_disciplines_text}`;
        str += `<h4>Адрес регистрации:</h4> ${this.location_reg.location_info()} <br><br>`;
        str += `<h4>Место проживания:</h4> ${this.current_location.current_location_info()} <br><br>`;

        label.innerHTML = str;
    }
}

student.show();