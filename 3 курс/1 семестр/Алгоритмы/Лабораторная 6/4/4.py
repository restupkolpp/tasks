def substring(string, sim):
    dct = {}
    for word in sim:
        cell = [[0] * len(word) for m in range(len(string))]
        for i in range(len(string)):
            for j in range(len(word)):
                if string[i] == word[j]:
                    cell[i][j] = cell[i - 1][j - 1] + 1
                else:
                    cell[i][j] = 0
        dct[word] = max(list(map(max, cell)))

    return max(dct, key=dct.get)


userString = input("Введите строку: ")
sim = []
count = int(input("Введите количество похожих слов: "))

print("Введите похожие слова:")
for i in range(count):
    sim.append(input())

print(f'Самое похожее слово: {substring(userString, sim)}')
