import itertools
import string
import timeit
import random
import matplotlib.pyplot as plt


def full_search(maxWeight, items):
    final_weight = 0
    final_cost = 0
    final_items = []

    for i in range(1, len(items) + 1):
        for _ in itertools.combinations(items, i):
            weight = sum(item['weight'] for item in _)
            cost = sum(item['cost'] for item in _)

            if weight <= maxWeight and cost > final_cost:
                final_weight = weight
                final_cost = cost
                final_items = _
    return final_items, final_weight, final_cost


def greedy_search(maxWeight, items):
    items_cp = items[:]
    items_cp.sort(key=lambda x: x['cost'] / x['weight'], reverse=True)

    res_weight = 0
    res_cost = 0
    res_items = []

    for item in items_cp:
        if res_weight + item['weight'] <= maxWeight:
            res_items.append(item['title'])
            res_weight += item['weight']
            res_cost += item['cost']

    return res_items, res_weight, res_cost


def dynamic_search(maxWeight, items):
    n = len(items)
    table = [[0] * (maxWeight + 1) for i in range(n + 1)]

    for i in range(1, n + 1):
        for j in range(1, maxWeight + 1):
            weight = items[i - 1]['weight']
            cost = items[i - 1]['cost']
            if weight > j:
                table[i][j] = table[i - 1][j]
            else:
                table[i][j] = max(table[i - 1][j], table[i - 1][j - weight] + cost)

    res_items = []
    res_weight = 0
    res_cost = 0
    i, j = n, maxWeight
    while i > 0 and j > 0:
        if table[i][j] != table[i - 1][j]:
            res_items.append(items[i - 1]['title'])
            res_weight += items[i - 1]['weight']
            res_cost += items[i - 1]['cost']
            j -= items[i - 1]['weight']
        i -= 1

    return res_items[::-1], res_weight, res_cost


maxWeight = int(input("Введите максималный вес: "))
n = int(input("Введите количество предметов: "))

itemList = []
for i in range(0, n):
    itemName = input("Введите имя предмета: ")
    itemCost = float(input("Введите цену предмета: "))
    itemWeight = int(input("Введите вес предмета: "))
    print()
    itemList.append({
        "title": itemName,
        "cost": itemCost,
        "weight": itemWeight
    })

final_items, final_weight, final_cost = full_search(maxWeight=maxWeight, items=itemList)
print(final_items)
print(final_weight, final_cost)

print()
final_items, final_weight, final_cost = greedy_search(maxWeight=maxWeight, items=itemList)
print(final_items)
print(final_weight, final_cost)

print()
final_items, final_weight, final_cost = dynamic_search(maxWeight=maxWeight, items=itemList)
print(final_items)
print(final_weight, final_cost)

plt_x = []
plt_search = []
plt_greedy = []
plt_dynamic = []
time_search = timeit.Timer('full_search(v, items)', globals=globals())
time_greedy = timeit.Timer('greedy_search(v, items)', globals=globals())
time_dynamic = timeit.Timer('dynamic_search(v, items)', globals=globals())

for i in range(1, 10):
    plt_x.append(i)
    v = random.randint(1, 100)
    items = [{'title': ''.join(random.choice(string.ascii_lowercase) for m in range(v)),
              'weight': random.randint(1, 10),
              'cost': random.randint(1, 20)} for j in range(i)]

    time1 = time_search.timeit(number=1)
    plt_search.append(time1)

    time2 = time_greedy.timeit(number=1)
    plt_greedy.append(time2)

    time3 = time_dynamic.timeit(number=1)
    plt_dynamic.append(time3)

print(plt_greedy)

plt.plot(plt_x, plt_search, label='Full Search')
plt.plot(plt_x, plt_greedy, label='Greedy algorithm')
plt.plot(plt_x, plt_dynamic, label='Dynamic programming')
plt.xlabel('Items length')
plt.ylabel('Runtime')
plt.legend()
plt.show()
