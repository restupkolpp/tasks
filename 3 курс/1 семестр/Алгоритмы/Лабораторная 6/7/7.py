def greedy_alg(n):
    money = [1, 3, 4, 10, 50, 100]
    dct = {}
    for i in money[::-1]:
        multiplier = n // i
        n -= multiplier * i
        dct[i] = multiplier
    return dct


def dynamic_alg(n):
    coins = [1, 3, 4, 10, 50, 100]
    table = [0 if i == 0 else float('inf') for i in range(n + 1)]
    for i in range(1, n + 1):
        for j in coins:
            if i - j >= 0:
                table[i] = min(table[i], table[i - j] + 1)
    return table[n]


n = int(input("Введите количество денег, которое нужно снять: "))
print('Жадный алгоритм: ', greedy_alg(n))
print('Динамическое программирование: ', dynamic_alg(n))
