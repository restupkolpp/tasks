import random


def first_right_box(items):
    boxes = [[]]
    for item in items:
        for box in boxes:
            if sum(box) + item <= 1.0:
                box.append(item)
                break
        else:
            boxes.append([item])
    return len(boxes)


def most_right_box(items):
    boxesList = [[]]
    for i in range(0, len(items)):
        currentItem = items[i]
        remainingSpaceAfterPut = [0]*len(boxesList)

        for i in range(0, len(boxesList)):
            boxWeight = sum(boxesList[i])
            remainingSpaceAfterPut[i] = 1.0 - (boxWeight + currentItem)

        remainingSpaceAfterPut = [i if i >= 0 else 1000 for i in remainingSpaceAfterPut]

        min_value = min(remainingSpaceAfterPut)
        min_idx = remainingSpaceAfterPut.index(min_value)

        if min_value == 1000:
            min_idx = len(boxesList)
            boxesList.append([])
        boxesList[min_idx].append(currentItem)
    return len(boxesList)


def next_right_box(items):
    boxes = []
    box = []
    for item in items:
        if sum(box) + item <= 1.0:
            box.append(item)
        else:
            boxes.append(box)
            box = [item]
    boxes.append(box)
    return len(boxes)


def min_right_box(items):
    boxes = [[]]
    for item in items:
        max_space = 0
        max_box = None
        for box in boxes:
            space_left = 1.0 - sum(box)
            if max_space < space_left >= item:
                max_space = space_left
                max_box = box
        if max_box is not None:
            max_box.append(item)
        else:
            boxes.append([item])
    return len(boxes)


for i in [50, 100, 200, 500]:
    print(f'i = {i}')
    items = tuple([random.uniform(0.1, 1.0) for i in range(i)])
    print(f'Первый подходящий ящик: {first_right_box(items)}')
    print(f'Наиболее подходящий ящик: {most_right_box(items)}')
    print(f'Следующий подходящий ящик: {next_right_box(items)}')
    print(f'Наименее подходящий ящик: {min_right_box(items)}')
    print()

