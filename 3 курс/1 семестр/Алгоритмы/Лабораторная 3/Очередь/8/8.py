# Вариант 1. Машины, стоящие в очереди в мойку.
import random


class Queue:
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def enqueue(self, item):
        self.items.insert(0, item)

    def dequeue(self):
        return self.items.pop()

    def size(self):
        return len(self.items)


class CarWash:
    def __init__(self, cpm):
        self.carrate = cpm
        self.currentTask = None
        self.timeRemaining = 0

    def tick(self):
        if self.currentTask is not None:
            self.timeRemaining = self.timeRemaining - 1
            if self.timeRemaining <= 0:
                self.currentTask = None

    def busy(self):
        if self.currentTask is not None:
            return True
        else:
            return False

    def startNext(self, newtask):
        self.currentTask = newtask
        self.timeRemaining = newtask.getPages() * 60 / self.carrate


class Task:
    def __init__(self, time):
        self.timestamp = time
        self.cars = random.randrange(1, 21)

    def getStamp(self):
        return self.timestamp

    def getPages(self):
        return self.cars

    def waitTime(self, currenttime):
        return currenttime - self.timestamp


def simulation(numMinutes, carsPerHour):
    carWash = CarWash(carsPerHour)
    carWashQueue = Queue()
    waitingtimes = []

    for currentMinutes in range(numMinutes):

        if newCar():
            task = Task(currentMinutes)
            carWashQueue.enqueue(task)

        if (not carWash.busy()) and (not carWashQueue.isEmpty()):
            nexttask = carWashQueue.dequeue()
            waitingtimes.append(nexttask.waitTime(currentMinutes))
            carWash.startNext(nexttask)

        carWash.tick()

    averageWait = (sum(waitingtimes) / len(waitingtimes)).__round__()
    print("Время, затраченное на мойку машины %6.2f мин %3d машин не успели помыть." % (averageWait, carWashQueue.size()))


def newCar():
    num = random.randrange(1, 181)
    if num == 180:
        return True
    else:
        return False


for i in range(10):
    simulation(3600, 5)
