from lists.UnorderedList import UnorderedList

mylist = UnorderedList()

mylist.add(31)
mylist.add(77)
mylist.add(17)
mylist.add(93)
mylist.add(26)
mylist.add(54)

print(mylist)

print("Размер списка:", mylist.size())
mylist.append(65)
print()

print("Размер списка после вызова метода append:", mylist.size())
print(mylist)
print()

print("Индекс числа 77:", mylist.index(77))
print(mylist)
print()

mylist.insert(5, 90)
print("Размер списка после вызова метода insert:", mylist.size())
print(mylist)
print()

mylist.pop()
print("Размер списка после вызова метода pop:", mylist.size())
print(mylist)
print()

mylist.pop(2)
print("Размер списка после вызова метода pop(pos):", mylist.size())
print(mylist)
print()

print(mylist)
print()
