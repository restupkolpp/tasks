from lists.Stack import Stack

import re

text = '''
<html>
    <head>
        <title>
            Example
        </title>
    </head>
    <body>
        <h1>Hello, world</h1>
    </body>
</html>'''


def check_html_balance(txt):
    s = Stack()
    x = txt.replace('<', ' <')
    y = x.replace('>', '> ')
    a = [w for w in y.split() if re.search('<\S+>', w)]
    b = [w for w in a if "/" not in w]
    c = [w for w in a if "/" in w]
    for w in txt.split():
        if w in b:
            s.push(w)
        elif not s.isEmpty() and (w in c) and (w.replace('/', '') == s.peek()):
            s.pop()
    return s.isEmpty()


print(check_html_balance(text))
