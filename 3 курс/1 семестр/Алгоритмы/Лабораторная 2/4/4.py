import timeit as timeit
import matplotlib.pyplot as plt


x = [i for i in range(10, 41, 10)]
fib_time = []
fib_with_lucas_time = []


def fibonacci(n):
    if n <= 0:
        return 0
    if n == 1:
        return 1
    return fibonacci(n - 1) + fibonacci(n - 2)


def lucas(n):
    if n < 0:
        return 0
    if n == 0:
        return 2
    if n == 1:
        return 1
    return lucas(n - 1) + lucas(n - 2)


def fib_with_lucas(n):
    i = n // 2
    j = n - i
    return ((fibonacci(i) + lucas(j)) * (fibonacci(j) + lucas(i))) // 2


def lucas_with_fib(n):
    return fibonacci(n - 1) + fibonacci(n + 1)


fib_40_time = timeit.timeit("fibonacci(40)", number=1, globals=globals())

for i in x:
    fib_time.append(timeit.timeit(f'fibonacci({i})', number=1, globals=globals()))
    fib_with_lucas_time.append(timeit.timeit(f'fib_with_lucas({i})', number=1, globals=globals()))

plt.plot(x, fib_time, label='Fib')
plt.plot(x, fib_with_lucas_time, label='Fib with Lucas')

plt.ylabel("Time")
plt.xlabel("Elements")
plt.legend()

plt.show()
