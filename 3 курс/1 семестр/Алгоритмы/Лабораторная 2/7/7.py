import random
import turtle as t


def draw(length, iterations, goto_x, goto_y):
    if iterations != 0:
        t.left(60)
        t.forward(length)
        t.right(120)
        t.forward(length * random.uniform(0.4, 0.9) + 40)
        t.left(60)
        draw(length * random.uniform(0.7, 1.4), iterations - 1, goto_x + (length * random.uniform(0.2, 0.4)), goto_y)


def start(x):
    t.clear()
    t.penup()
    x = x if x < 0 else -x
    t.goto(x, -200)
    t.pendown()
    draw(250, 3, x, -250)

    t.exitonclick()


start(-400)
