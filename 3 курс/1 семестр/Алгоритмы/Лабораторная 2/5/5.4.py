import random
import turtle


def tree(branchLen, t):
    t.speed(100000)
    if branchLen > 5:

        angle = (random.randint(10, 40) + 5) * 1.5
        branchLenDiff = random.randint(1, 10) + 7

        t.width(5)
        if branchLen < 15:
            t.color('green')
        else:
            t.color('brown')

        t.forward(branchLen)
        t.right(angle)
        tree(branchLen - branchLenDiff, t)
        t.left(angle * 2)
        tree(branchLen - branchLenDiff, t)
        t.right(angle)
        t.backward(branchLen)
        t.color('brown')


def main():
    t = turtle.Turtle()
    myWin = turtle.Screen()
    t.color('brown')
    t.width(6)
    t.left(90)
    t.up()
    t.backward(100)
    t.down()
    tree(75, t)
    myWin.exitonclick()


main()
