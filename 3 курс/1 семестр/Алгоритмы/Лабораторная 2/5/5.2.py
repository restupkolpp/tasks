import turtle


def tree(branchLen, t):
    if branchLen > 5:
        t.width(4)
        if branchLen == 15:
            t.color('green')
        else:
            t.color('brown')

        t.forward(branchLen)
        t.right(20)
        tree(branchLen - 15, t)
        t.left(40)
        tree(branchLen - 15, t)
        t.right(20)
        t.backward(branchLen)
        t.color('brown')


def main():
    t = turtle.Turtle()
    myWin = turtle.Screen()
    t.color('brown')
    t.width(6)
    t.left(90)
    t.up()
    t.backward(100)
    t.down()
    tree(75, t)
    myWin.exitonclick()


main()
