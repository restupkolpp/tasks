import numpy as np

sudoku = [
    [0, 0, 0, 0],
    [0, 0, 2, 0],
    [0, 1, 0, 0],
    [3, 0, 0, 4]
]


def checkrow(arr, x, y, n):
    fitsFlag = True
    row = arr[x]

    if n in row:
        fitsFlag = False

    return fitsFlag


def checkcol(arr, x, y, n):
    fitsFlag = True

    col = []
    for i in range(0, len(arr) - 1):
        col.append(arr[i][y])

    if n in col:
        fitsFlag = False

    return fitsFlag


def checksquare(arr, x, y, n):
    fitsFlag = True

    arr_cpy_np = np.array(arr)

    sq_idx_x = 0 if x <= 1 else 2
    sq_idx_y = 0 if y <= 1 else 2

    sq_matr = arr_cpy_np[sq_idx_x:sq_idx_x + 2, sq_idx_y:sq_idx_y + 2]

    if n in sq_matr:
        fitsFlag = False

    return fitsFlag


def possible(arr, x, y, n):
    return checkrow(arr, x, y, n) and checkcol(arr, x, y, n) and checksquare(arr, x, y, n)


def solve(arr):
    for x in range(4):
        for y in range(4):
            if arr[x][y] == 0:
                for n in range(1, 5):
                    if possible(arr, x, y, n):
                        arr[x][y] = n
                        solve(arr)
                        arr[x][y] = 0
                return
    print(np.matrix(arr))


solve(sudoku)
