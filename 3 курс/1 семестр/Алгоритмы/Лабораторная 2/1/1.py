import random
import timeit
import matplotlib.pyplot as plt


def selection_sort(arr):
    for i in range(0, len(arr) - 1):
        smallest = i
        for j in range(i + 1, len(arr)):
            if arr[j] < arr[smallest]:
                smallest = j
        arr[i], arr[smallest] = arr[smallest], arr[i]
    return arr


def quick_sort(arr):
    if len(arr) <= 1:
        return arr
    pivot = arr[len(arr) // 2]
    less = [x for x in arr[1:] if x <= pivot]
    greater = [x for x in arr[1:] if x > pivot]
    return quick_sort(less) + [pivot] + quick_sort(greater)


x = [i for i in range(1000, 10001, 1000)]

quick_sorted = []
quick_random = []
quick_reversed = []

select_sorted = []
select_random = []
select_reversed = []

for i in x:
    arr_1 = [random.randint(-1000, 1000) for j in range(1, i)]
    arr_2 = [random.randint(-1000, 1000) for j in range(1, i)]
    quick_random.append(timeit.timeit("quick_sort(arr_1)", number=1, globals=globals()))
    select_random.append(timeit.timeit("selection_sort(arr_2)", number=1, globals=globals()))

    arr_1 = quick_sort(arr_1)
    selection_sort(arr_2)

    quick_sorted.append(timeit.timeit("quick_sort(arr_1)", number=1, globals=globals()))
    select_sorted.append(timeit.timeit("selection_sort(arr_2)", number=1, globals=globals()))

    arr_1.sort(reverse=True)
    arr_2.sort(reverse=True)

    quick_reversed.append(timeit.timeit("quick_sort(arr_1)", number=1, globals=globals()))
    select_reversed.append(timeit.timeit("selection_sort(arr_1)", number=1, globals=globals()))

plt.ylabel('Time')
plt.xlabel('Numbers')

plt.plot(x, quick_random, label='Quick, Random')
plt.plot(x, select_random, label='Select, Random')

plt.plot(x, quick_sorted, label='Quick, Sorted')
plt.plot(x, select_sorted, label='Select, Sorted')

plt.plot(x, quick_reversed, label='Quick, Reversed')
plt.plot(x, select_reversed, label='Select, Reversed')

plt.legend()
plt.show()
