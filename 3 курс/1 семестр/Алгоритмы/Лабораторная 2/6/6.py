import turtle as t


def koch_curve(size, n):
    if n == 0:
        t.forward(size)
    else:
        koch_curve(size / 3, n - 1)
        t.left(60)
        koch_curve(size / 3, n - 1)
        t.right(120)
        koch_curve(size / 3, n - 1)
        t.left(60)
        koch_curve(size / 3, n - 1)


def start(x: float):
    t.speed(1000)
    t.clear()
    t.penup()
    x = x if x < 0 else -x
    t.goto(x, 0)
    t.pendown()
    koch_curve(300, 4)
    t.exitonclick()


start(300)
