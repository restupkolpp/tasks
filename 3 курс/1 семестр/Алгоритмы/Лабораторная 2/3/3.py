def recursive_poly(word):
    if len(word) <= 1:
        return "Yes"
    if word[0] == word[-1]:
        return recursive_poly(word[1:-1])
    else:
        return 'No'


print(recursive_poly("non"))
