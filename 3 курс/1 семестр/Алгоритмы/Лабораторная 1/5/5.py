import timeit
import matplotlib.pyplot as plt

time_list = timeit.Timer("100000 in my_list", globals=globals())
time_set = timeit.Timer("100000 in my_set", globals=globals())

plt_x = []
plt_list = []
plt_set = []

for i in range(1000, 10001, 1000):
    plt_x.append(i)

    my_list = list(range(i))
    pt = time_list.timeit(number=1)
    plt_list.append(pt)

    my_set = set(range(i))
    pz = time_set.timeit(number=1)
    plt_set.append(pz)

    print("%15.5f, %15.5f" % (pz, pt))

plt.ylabel('Time')
plt.xlabel('Numbers')

plt.plot(plt_x, plt_list, label='List; Complexity - O(N)')
plt.plot(plt_x, plt_set, label='Set; Complexity - O(1)')

plt.legend()
plt.show()
