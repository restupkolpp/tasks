import timeit
import matplotlib.pyplot as plt


def delListItems(lis):
    del lis[1]
    return lis


def delDictItems(dic):
    del dic[str('34519')]
    return dic


b = []
c = {}

time_list = timeit.Timer('delListItems(b)', globals=globals())
time_dict = timeit.Timer('delDictItems(c)', globals=globals())

plt_x = []
plt_list = []
plt_dict = []

for x in range(100000, 1000001, 100000):
    plt_x.append(x)

    b = sorted([x for x in range(x)], reverse=True)
    c = {str(j): j for j in range(0, x)}
    pt_list = time_list.timeit(number=1)
    plt_list.append(pt_list)

    pt_dict = time_dict.timeit(number=1)
    plt_dict.append(pt_dict)

    print("%15.5f, %15.5f" % (pt_list, pt_dict))

plt.ylabel('Time')
plt.xlabel('Numbers')

plt.plot(plt_x, plt_list, label='Del from list')
plt.plot(plt_x, plt_dict, label='Del from dict')

plt.legend()
plt.show()
