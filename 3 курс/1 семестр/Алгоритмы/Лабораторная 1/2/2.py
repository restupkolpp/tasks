import timeit
import matplotlib.pyplot as plt


def maxNumber(a):
    return max(a)


def minNumber(a):
    return min(a)


def mechMin(a):
    m = a[0]
    for elem in a:
        if elem < m:
            m = elem
    return m


def mechMax(a):
    m = a[0]
    for elem in a:
        if elem > m:
            m = elem
    return m


b = []

time_max = timeit.Timer('maxNumber(b)', globals=globals())
time_min = timeit.Timer('minNumber(b)', globals=globals())
time_mechMax = timeit.Timer('mechMax(b)', globals=globals())
time_mechMin = timeit.Timer('mechMin(b)', globals=globals())

plt_x = []
plt_max = []
plt_min = []
plt_mechMax = []
plt_mechMin = []

for x in range(1000, 10001, 1000):
    plt_x.append(x)

    b = sorted([x for x in range(x)], reverse=True)

    pt_max = time_max.timeit(number=1)
    plt_max.append(pt_max)

    pt_min = time_min.timeit(number=1)
    plt_min.append(pt_min)

    pt_mechMax = time_mechMax.timeit(number=1)
    plt_mechMax.append(pt_mechMax)

    pt_mechMin = time_mechMin.timeit(number=1)
    plt_mechMin.append(pt_mechMin)

    print("%15.5f, %15.5f, %15.5f, %15.5f" % (pt_max, pt_min, pt_mechMax, pt_mechMin))


plt.ylabel('Time')
plt.xlabel('Numbers')

plt.plot(plt_x, plt_max, label='Python max; Complexity - O(log(N))')
plt.plot(plt_x, plt_min, label='Python min; Complexity - O(log(N))')
plt.plot(plt_x, plt_mechMax, label='Manual search max; Complexity - O(N)')
plt.plot(plt_x, plt_mechMin, label='Manual search min; Complexity - O(N)')

plt.legend()
plt.show()
