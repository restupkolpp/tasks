import timeit
import matplotlib.pyplot as plt

# Данная функция сортирует массив


def foo(a):
    for i in range(len(a), 0, -1):
        for j in range(1, i):
            if a[j - 1] > a[j]:
                a[j - 1], a[j] = a[j], a[j - 1]
    return a


b = []

time_sort = timeit.Timer('foo(b)', globals=globals())

plt_x = []
plt_list = []

for x in range(1000, 10001, 1000):
    plt_x.append(x)

    b = sorted([x for x in range(x)], reverse=True)
    pt = time_sort.timeit(number=1)
    plt_list.append(pt)

    print("%15.5f" % pt)

plt.ylabel('Time')
plt.xlabel('Numbers')

line = plt.plot(plt_x, plt_list, label='O(N^2)')

plt.legend()
plt.show()
