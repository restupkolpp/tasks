from MaxHeap import MaxHeap


heap = MaxHeap()
alist = [5, 60, 20, 23, 45, 79, 30, 35, 65, 80, 45]

heap.buildHeap(alist)
print(list(reversed(sorted(alist))))
print("size:", heap.currentSize)
print(heap.delMax())
print(heap.delMax())
print(heap.delMax())
print(heap.delMax())
print(heap.delMax())
print(heap.delMax())
