from BoundedMinHeap import MinHeap


heap = MinHeap(6)
alist = [5, 60, 20, 23, 45, 79, 30, 35, 65, 80, 45]

heap.buildHeap(alist)

print("maxSize:", heap.maxElements)
print("size:", heap.currentSize)
print(heap.delMin())
print(heap.delMin())
print(heap.delMin())
print(heap.delMin())
print(heap.delMin())
print(heap.delMin())
