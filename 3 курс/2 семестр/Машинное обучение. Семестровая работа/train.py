from keras_preprocessing import image
from keras.models import load_model
from keras.applications.vgg16 import preprocess_input
import numpy as np

model = load_model('our_model.h5', compile=False)
img = image.load_img('chest_xray/val/PNEUMONIA/person1946_bacteria_4874.jpeg',
                     target_size=(224, 224))

imagee = image.img_to_array(img)
imagee = np.expand_dims(imagee, axis=0)
img_data = preprocess_input(imagee)

prediction = model.predict(img_data)
if prediction[0][0] > prediction[0][1]:
    print('Пациент здоров.')
else:
    print('У пациента пневмония.')
print(f'Predictions: {prediction}')
