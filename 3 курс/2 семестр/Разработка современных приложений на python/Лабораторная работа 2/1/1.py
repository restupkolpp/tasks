import sys

arguments = {}

for arg in sys.argv[1:]:
    if '=' in arg:
        key, value = arg.split('=')
        arguments[key] = value


if '--sort' in sys.argv[1:]:
    sorted_keys = sorted(arguments.keys())
    for key in sorted_keys:
        print(f"Key: {key} Value: {arguments[key]}")
else:
    for key, value in arguments.items():
        print(f"Key: {key} Value: {value}")
