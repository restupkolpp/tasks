from flask import Flask, render_template, redirect, request, session, url_for

from data import db_session
from data.task import Task
from data.user import User
from flask_restful import Api
from task_api import TaskResource

app = Flask(__name__)
app.config['SECRET_KEY'] = 'my_secret_key'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///tasks.db'
api = Api(app)


@app.route('/')
def index():
    if 'username' in session:
        db_session.global_init("db/tasks_info.db")
        db_sess = db_session.create_session()
        username = session['username']
        user = db_sess.query(User).filter_by(username=username).first()
        user_tasks = user.tasks
        return render_template('tasks_page.html', username=username, tasks=user_tasks)
    return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        db_session.global_init("db/tasks_info.db")
        db_sess = db_session.create_session()
        username = request.form['username']
        password = request.form['password']
        user = db_sess.query(User).filter_by(username=username, hashed_password=password).first()
        if user:
            session['username'] = username
            return redirect(url_for('index'))
        else:
            return render_template('login.html', error='Invalid credentials')
    return render_template('login.html')


@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        db_session.global_init("db/tasks_info.db")
        db_sess = db_session.create_session()
        username = request.form['username']
        password = request.form['password']
        existing_user = db_sess.query(User).filter_by(username=username).first()
        if existing_user:
            return "Пользователь с таким именем уже существует"
        new_user = User(username=username, hashed_password=password)
        db_sess.add(new_user)
        db_sess.commit()
        return redirect(url_for('login'))
    return render_template('register.html')


@app.route('/add_task', methods=['POST'])
def add_task():
    if 'username' in session:
        db_session.global_init("db/tasks_info.db")
        db_sess = db_session.create_session()
        username = session['username']
        user = db_sess.query(User).filter_by(username=username).first()
        task = request.form['task']
        new_task = Task(task=task, user=user)
        db_sess.add(new_task)
        db_sess.commit()
    return redirect(url_for('index'))


@app.route('/logout')
def logout():
    session.pop('username', None)
    return redirect(url_for('index'))


@app.route('/complete_task/<int:task_id>')
def complete_task(task_id):
    if 'username' in session:
        db_session.global_init("db/tasks_info.db")
        db_sess = db_session.create_session()
        username = session['username']
        user = db_sess.query(User).filter_by(username=username).first()
        task = db_sess.query(Task).filter_by(id=task_id, user=user).first()
        if task:
            task.completed = True
            db_sess.commit()
    return redirect(url_for('index'))


@app.route('/edit_task/<int:task_id>', methods=['GET', 'POST'])
def edit_task(task_id):
    if 'username' in session:
        db_session.global_init("db/tasks_info.db")
        db_sess = db_session.create_session()
        username = session['username']
        user = db_sess.query(User).filter_by(username=username).first()
        task = db_sess.query(Task).filter_by(id=task_id, user=user).first()
        if task:
            if request.method == 'POST':
                task.task = request.form['task']
                db_sess.commit()
                return redirect(url_for('index'))
            return render_template('edit_task.html', task=task)
    return redirect(url_for('index'))


@app.route('/delete_task/<int:task_id>')
def delete_task(task_id):
    if 'username' in session:
        db_session.global_init("db/tasks_info.db")
        db_sess = db_session.create_session()
        username = session['username']
        user = db_sess.query(User).filter_by(username=username).first()
        task = db_sess.query(Task).filter_by(id=task_id, user=user).first()
        if task:
            db_sess.delete(task)
            db_sess.commit()
    return redirect(url_for('index'))


@app.route('/filter_tasks/<string:status>')
def filter_tasks(status):
    if 'username' in session:
        db_session.global_init("db/tasks_info.db")
        db_sess = db_session.create_session()
        username = session['username']
        user = db_sess.query(User).filter_by(username=username).first()
        if status == 'completed':
            filtered_tasks = db_sess.query(Task).filter_by(user=user, completed=True).all()
        elif status == 'incomplete':
            filtered_tasks = db_sess.query(Task).filter_by(user=user, completed=False).all()
        else:
            filtered_tasks = user.tasks
        return render_template('tasks_page.html', username=username, tasks=filtered_tasks)
    return redirect(url_for('login'))


api.add_resource(TaskResource, '/api/tasks', '/api/tasks/<int:task_id>')

if __name__ == '__main__':
    app.run(port=8080, host='127.0.0.1')
