from flask_wtf import FlaskForm
from wtforms import SubmitField, BooleanField, StringField
from wtforms.validators import DataRequired


class AddTask(FlaskForm):
    task = StringField('Название задачи', validators=[DataRequired()])
    completed = BooleanField('Выполнена ли задача')
    submit = SubmitField('Добавить')
