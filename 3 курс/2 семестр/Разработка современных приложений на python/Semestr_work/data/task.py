import sqlalchemy
import datetime
from sqlalchemy import orm

from .db_session import SqlAlchemyBase


class Task(SqlAlchemyBase):
    __tablename__ = 'task'

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    task = sqlalchemy.Column(sqlalchemy.String(200))
    completed = sqlalchemy.Column(sqlalchemy.Boolean, default=False)
    date = sqlalchemy.Column(sqlalchemy.Date, default=datetime.datetime.now())
    user_id = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey('user.id'))

    def __repr__(self):
        return f'<Task> {self.title}'
