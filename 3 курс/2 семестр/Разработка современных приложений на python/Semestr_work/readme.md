# Todo project

This is my project that does the following:

- Implements an API for task management.
- Allows you to create, retrieve, update and delete tasks.

## There are two tables in the program:

- Task
- User

The first one stores information about the tasks added by the user
The second is used to store users

## Tech

The application uses a number of libraries to work properly:

- Flask
- Flask_RESTful
- flask_wtf
- SQLAlchemy
- Werkzeug
- WTForms