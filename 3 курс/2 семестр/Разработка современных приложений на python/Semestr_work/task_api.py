from flask import request, session

from data.task import Task
from data.user import User
from data import db_session
from flask_restful import Resource


class TaskResource(Resource):
    def get(self, task_id=None):
        db_session.global_init("db/tasks_info.db")
        db_sess = db_session.create_session()
        if task_id:
            task = db_sess.query(Task).get(task_id)
            if task:
                return {'task': task.task, 'completed': task.completed, 'date': task.date}
            else:
                return {'error': 'Task not found'}, 404
        else:
            tasks = db_sess.query(Task).all()
            return [{'task': task.task, 'completed': task.completed, 'date': task.date} for task in tasks]

    def post(self):
        db_session.global_init("db/tasks_info.db")
        db_sess = db_session.create_session()
        task = request.form['task']
        user = User.query.filter_by(username=session['username']).first()
        new_task = Task(task=task, user=user)
        db_sess.add(new_task)
        db_sess.commit()
        return {'message': 'Task created successfully'}, 201

    def put(self, task_id):
        db_session.global_init("db/tasks_info.db")
        db_sess = db_session.create_session()
        task = db_sess.query(Task).get(task_id)
        if task:
            task.completed = True
            db_sess.commit()
            return {'message': 'Task updated successfully'}
        else:
            return {'error': 'Task not found'}, 404

    def delete(self, task_id):
        db_session.global_init("db/tasks_info.db")
        db_sess = db_session.create_session()
        task = Task.query.get(task_id)
        if task:
            db_sess.delete(task)
            db_sess.commit()
            return {'message': 'Task deleted successfully'}
        else:
            return {'error': 'Task not found'}, 404
