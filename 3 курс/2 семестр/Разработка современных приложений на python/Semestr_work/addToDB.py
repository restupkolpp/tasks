import datetime

from flask import Flask
from data import db_session
from data.task import Task
from data.user import User

app = Flask(__name__)
app.config['SECRET_KEY'] = 'my_secret_key'


def add_user(username):
    user = User()

    user.username = username

    db_sess = db_session.create_session()
    db_sess.add(user)
    db_sess.commit()


def add_task(title, user_id, completed, date):
    task = Task()

    task.task = title
    task.user_id = user_id
    task.completed = completed
    task.date = date

    db_sess = db_session.create_session()
    db_sess.add(task)
    db_sess.commit()


def main():
    db_session.global_init("db/tasks_info.db")

    app.run()


if __name__ == '__main__':
    main()
