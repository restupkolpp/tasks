from flask_wtf import FlaskForm
from flask_wtf.file import FileRequired, FileAllowed
from wtforms import StringField, BooleanField, SubmitField, EmailField, SelectField, RadioField, TextAreaField, \
    FileField
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
    last_name = StringField('Фамилия', validators=[DataRequired()])
    first_name = StringField('Имя', validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired()])
    education = SelectField('Образование', choices=["Высшее", "Среднее", "9 классов"])
    profession = SelectField('Выбор основной профессии', choices=["Инженер-исследователь",
                                                                  "Пилот", "Строитель",
                                                                  "Экзобиолог", "Врач",
                                                                  "Инженер по терраформированию",
                                                                  "Климатолог", "Специалист по радиационной защите",
                                                                  "Астрогеолог", "Гляциолог",
                                                                  "Инженер жизнеобеспечения",
                                                                  "Метеоролог", "Оператор марсохода",
                                                                  "Киберинженер", "Штурман", "Пилот дронов"])
    sex = RadioField('Пол', choices=["Мужской", "Женский"])
    motivation = TextAreaField('Ваша мотивация')
    is_ready = BooleanField('Готовы ли вы остаться на марсе?')
    photo = FileField('Прикрепите фото:', validators=[FileRequired(), FileAllowed(['jpg', 'jpeg', 'png', 'gif'])])
    submit = SubmitField('Отправить')
