import json
import random
import smtplib
import time
import email

from flask import Flask, render_template, request, redirect

from Lab_4.task.loginform import LoginForm

app = Flask(__name__, static_folder="./members/photo")

app.config['SECRET_KEY'] = 'my_secret_key'

images = ['mars.png', 'mars1.png', 'mars2.png']


@app.route('/')
@app.route('/index')
def mission():
    return render_template("index.html", title="Добро пожаловать!")


@app.route('/list_prof/<list>')
def list_prof(list):
    professions = ['Пилот', 'Инженер', 'Техник', 'Врач', 'Переводчик', 'Электрик',
                   'Механик', 'Командир', 'Инструктор', 'Бортпроводник']
    if list == 'ol':
        return render_template('list_prof.html', title='Список профессий', professions=professions, list_type='ol')
    elif list == 'ul':
        return render_template('list_prof.html', title='Список профессий', professions=professions, list_type='ul')
    else:
        return f'''<h3>{'Неверный параметр'}'''


@app.route('/distribution')
def distribution():
    with open('members/persons.json', 'r', encoding='utf-8') as f:
        crew_data = json.load(f)
    return render_template('distribution.html', title='Размещение', crew_data=crew_data)


@app.route('/member/<int:number>')
def member_by_number(number):
    with open('members/persons.json', 'r', encoding='utf-8') as f:
        crew_data = json.load(f)

    if number is not None:
        if number < 1 or number > len(crew_data):
            return 'Ошибка: некорректный номер'
        member_data = crew_data[number - 1]

    return render_template('member.html', title='Член экипажа', member_data=member_data)


@app.route('/member/random')
def member_random():
    with open('members/persons.json', 'r', encoding='utf-8') as f:
        crew_data = json.load(f)
    member_data = random.choice(crew_data)
    return render_template('member.html', title='Член экипажа', member_data=member_data)


@app.route('/room/<sex>/<int:age>')
def room(sex, age):
    return render_template('room.html', title='Оформление каюты', sex=sex, age=age)


@app.route('/astronaut_selection', methods=['GET', 'POST'])
def astronaut_selection():
    if request.method == 'GET':
        form = LoginForm()
        if form.validate_on_submit():
            return '''<h1 >Данные успешно отправлены</h1>'''
        return render_template('astronaut_selection.html', title='Запись добровольцем', form=form)

    elif request.method == 'POST':
        print(request.form['last_name'])
        print(request.form['first_name'])
        print(request.form['email'])
        print(request.form['education'])
        print(request.form['profession'])
        print(request.form['sex'])
        print(request.form['motivation'])
        print(request.form.get('is_ready'))
        f = request.files['photo']
        print(f.filename)
        send_mail(request.form['last_name'], request.form['first_name'], request.form['email'],
                  request.form['education'], request.form['profession'], request.form['sex'],
                  request.form['motivation'],
                  request.form.get('is_ready'), f.filename)
    return redirect('/success')


@app.route('/success')
def success():
    return '''<h1 style="text-align: center; color: #d22e3a;">Форма отправлена</h1>'''


@app.route('/galery', methods=['GET', 'POST'])
def galery():
    if request.method == 'POST':
        image = request.files['image']
        images.append(image.filename)
    return render_template('galery.html', title='Галерея', images=images)


def send_mail(last_name, first_name, mail, education, profession, sex, motivation, is_ready, photo):
    ################# SMTP SSL ################################
    start = time.time()
    try:
        smtp_ssl = smtplib.SMTP_SSL('smtp.mail.ru', 465)
    except Exception as e:
        print("ErrorType : {}, Error : {}".format(type(e).__name__, e))
        smtp_ssl = None

    print("Connection Object : {}".format(smtp_ssl))
    print("Total Time Taken  : {:,.2f} Seconds".format(time.time() - start))

    ######### Log In to mail account ############################
    print("\nLogging In.....")
    resp_code, response = smtp_ssl.login(user="anoshin.antosha@mail.ru", password="Jr9wwVeXJuwBLTnSkWRr")

    print("Response Code : {}".format(resp_code))
    print("Response      : {}".format(response.decode()))

    ################ Send Mail ########################
    print("\nSending Mail..........")

    message = email.message.EmailMessage()
    message.set_default_type("text/plain")

    frm = "anoshin.antosha@mail.ru"
    to_list = "toni.anoshin.toni.anoshin@bk.ru"
    message["From"] = frm
    message["To"] = to_list
    message["Subject"] = "Test Email"

    body = f'''
    Фамилия: {last_name}
    Имя: {first_name}
    Почта: {mail}
    Образование: {education}
    Профессия: {profession}
    Пол: {sex}
    Мотивация: {motivation}
    Готовность остаться на марсе (y - да, n -нет): {is_ready} 
    '''
    message.set_content(body)

    ### Attach JPEG Image.
    with open(f"members/photo/{photo}", mode="rb") as fp:
        img_content = fp.read()
        message.add_attachment(img_content, maintype="image", subtype="png", filename="photo.png")

    response = smtp_ssl.send_message(from_addr=frm,
                                     to_addrs=to_list,
                                     msg=message)

    print("List of Failed Recipients : {}".format(response))

    ######### Log out to mail account ############################
    print("\nLogging Out....")
    resp_code, response = smtp_ssl.quit()

    print("Response Code : {}".format(resp_code))
    print("Response      : {}".format(response.decode()))


if __name__ == '__main__':
    app.run(port=8080, host='127.0.0.1')
