# y = x ** 2 + 10 * y ** 2
# y(x1) = 2 * x
# y(x2) = 20 * x2

from math import exp


def func(arr, n):
    t = 1
    h = 0.1 * (exp(-n/t))
    a1 = arr[0]
    a2 = arr[1]
    a1 = a1 - h * (2 * a1)
    a2 = a2 - h * (20 * a2)
    arr[0] = a1
    arr[1] = a2
    return arr


x = [10, 1]
for i in range(1, 200):
    x = func(x, i)
    print(x)
