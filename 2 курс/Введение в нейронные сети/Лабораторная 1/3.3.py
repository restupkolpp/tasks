def der(c):
    d = 0.01
    return ((c + d) * ((c + d) ** 2 - 9) - c * (c ** 2 - 9)) / d


def func(a):
    h = 0.1
    return a - h * der(a)


x = 0
y = x * (x ** 2 - 9)
y1 = 3 * x ** 2 - 9
x = func(-2)
for i in range(1, 2):
    x = func(x)
    print(x)