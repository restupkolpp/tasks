# y = x ** 2 + 10 * y ** 2
# y(x1) = 2 * x
# y(x2) = 20 * x2

from math import *


def func(arr, n):
    global Mn, Rn, mn, rn
    h = 0.1
    e = 0
    b1 = b2 = 0.9
    a1 = arr[0]
    a2 = arr[1]
    mn[0] = b1 * mn[0] + (1 - b1) * (2 * a1)
    mn[1] = b1 * mn[1] + (1 - b1) * (20 * a2)
    Mn[0] = mn[0] / (1 - pow(b1, n))
    Mn[1] = mn[1] / (1 - pow(b1, n))
    rn[0] = b2 * rn[0] + (1 - b2) * (2 * a1) ** 2
    rn[1] = b2 * rn[1] + (1 - b2) * (20 * a2) ** 2
    Rn[0] = rn[0] / (1 - pow(b2, n))
    Rn[1] = rn[1] / (1 - pow(b2, n))
    a1 = a1 - h * Mn[0] / sqrt(Rn[0] + e)
    a2 = a2 - h * Mn[1] / sqrt(Rn[1] + e)
    arr[0] = a1
    arr[1] = a2
    return arr


mn = [0, 0]
rn = [0, 0]
Mn = [0, 0]
Rn = [0, 0]
x = [10, 1]
for i in range(1, 1000):
    x = func(x, i)
    print(x)