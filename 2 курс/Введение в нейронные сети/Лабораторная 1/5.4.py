# y = x ** 2 + 10 * y ** 2
# y(x1) = 2 * x
# y(x2) = 20 * x2

from math import *


def dfx(a):
    return 2 * a


def dfy(a):
    return 20 * a


def func(arr):
    global g
    h = 0.1
    e = 0
    a1 = arr[0]
    a2 = arr[1]
    g1 = g[0] + dfx(a1) ** 2
    g2 = g[1] + dfy(a2) ** 2
    g = [g1, g2]
    a1 = a1 - h * dfx(a1) / sqrt(g[0] + e)
    a2 = a2 - h * dfy(a2) / sqrt(g[1] + e)
    arr[0] = a1
    arr[1] = a2
    return arr


g = [0, 0]
x = [10, 1]
for i in range(1, 10):
    x = func(x)
    print(x)
