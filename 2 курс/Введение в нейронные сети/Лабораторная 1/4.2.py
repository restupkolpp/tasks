# y = x1 ** 2 + 4 * x1 + 2 * x2 ** 2 + 5 * x2 + 3 * x1 * x2 + 6
# y(x1) = 2 * x1 + 4 + 3 * x2
# y(x2) = 2 * x2 + 5 + 3 * x1

from array import *


def func(arr):
    h = 0.1
    a1 = arr[0]
    a2 = arr[1]
    a1 = a1 - h * (2 * a1 + 4 + 3 * a2)
    a2 = a2 - h * (2 * a2 + 5 + 3 * a1)
    arr[0] = a1
    arr[1] = a2
    return arr


x = [-10, -10]

for i in range(1, 200):
    x = func(x)
    print(x)
