def der(c):
    d = 0.01
    return ((c + d) * ((c + d) ** 2 - 9) - c * (c ** 2 - 9)) / d


def func(a):
    h = 0.1
    return a - h * der(a)


def func1(a):
    h = 0.1
    return a - h * (3 * a ** 2 - 9)


x = 0
y = x * (x ** 2 - 9)
y1 = 3 * x ** 2 - 9
x = func(-1)
for i in range(1, 4):
    x = func(x)
    print("func: " + str(x))
y = func1(-1)
for i in range(1, 4):
    y = func1(y)
    print("func1: " + str(y))
