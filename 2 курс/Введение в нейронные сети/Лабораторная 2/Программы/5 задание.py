import math


def sigm(lx, lw0, lw1):
    return math.exp(lx * lw1 + lw0) / (1 + math.exp(lx * lw1 + lw0))


def hypertans(lx, lw0, lw1):
    return (math.exp(lx * lw1 + lw0) - math.exp(-1 * (lx * lw1 + lw0))) / \
        (math.exp(lx * lw1 + lw0) + math.exp(-1 * (lx * lw1 + lw0)))


def relu(lx, lw0, lw1):
    i = lx * lw0 + lw1
    if i <= 0:
        return 0
    else:
        return i


print("Выберите функцию активации, которую хотите использовать:")
print("1 - Сигмоида")
print("2 - Гиперболический тангенс")
print("3 - ReLU")

n = int(input())

print("Введите x, w0, w1: ")
x = int(input())
w0 = int(input())
w1 = int(input())

if n == 1:
    print(sigm(x, w0, w1))

if n == 2:
    print(hypertans(x, w0, w1))

if n == 3:
    print(relu(x, w0, w1))
