import math


def dFdw(x, w, w_index, index_1, index_2):
    h = 0.01
    w_h = dict(w)
    w_h[w_index] = w[w_index] + h
    return (math.log(1 + math.exp(x * w_h[index_1] + w_h[index_2])) - math.log(1 + math.exp(x * w[index_1] + w[index_2]))) / h


def dLw(wij, w, index):
    return sum([dFdw(wij[0][0], w, index, '2', '4'),
                dFdw(wij[1][0], w, index, '1', '3'),
                dFdw(wij[2][0], w, index, '2', '4')])


def func(wij, w):
    h = 0.1
    w['1'] = w['1'] - h * dLw(wij, w, '1')
    w['2'] = w['2'] - h * dLw(wij, w, '2')
    w['3'] = w['3'] - h * dLw(wij, w, '3')
    w['4'] = w['4'] - h * dLw(wij, w, '4')
    return w


def softmax(R, index):
    return math.exp(R[index]) / (math.exp(R[index]) + math.exp(R[abs(index - 1)]))


wij = [[-1, 0],
       [0, 1],
       [1, 0]]


w1 = {'1': 0, '2': 0, '3': 0, '4': 0}


def Pr0(x, w):
    return math.exp(x * w['1'] + w['3']) / (math.exp(x * w['1'] + w['3']) + math.exp(x * w['2'] + w['4']))


def Pr1(x, w):
    return math.exp(x * w['2'] + w['4']) / (math.exp(x * w['1'] + w['3']) + math.exp(x * w['2'] + w['4']))


for i in range(100):
    func(wij, w1)


print(w1['1'])
print(w1['2'])
print(w1['3'])
print(w1['4'])

print("x1 Pr0", Pr0(wij[0][0], w1))
print("x1 Pr1", Pr1(wij[0][0], w1))
print("x2 Pr0", Pr0(wij[1][0], w1))
print("x2 Pr1", Pr1(wij[1][0], w1))
print("x3 Pr0", Pr0(wij[2][0], w1))
print("x3 Pr1", Pr1(wij[2][0], w1))
