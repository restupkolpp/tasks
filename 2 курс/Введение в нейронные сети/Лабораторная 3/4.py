import random
#x = [-1, 0, 1]
#y = [1, 0, -1]
#Lw = (-w1 + w0 - 1) ** 2 + (w0 - 0) ** 2 + (w1 + w0 + 1) ** 2


def Calc(w0, w1):
    Arr = [0, 0]
    Lwdw1 = [(2 * w1 - 2 * w0 + 2), 0, (2 * w1 + 2 * w0 + 2)]
    Lwdw0 = [(-2 * w1 - 2 * w0 - 2), (2 * w0), (2 * w1 + 2 * w0 + 2)]
    Arr[0] = Lwdw0
    Arr[1] = Lwdw1
    return Arr


def func(wi, i):
    global Lwdw01
    h = 0.1
    if i != 1:
        wi[0] = wi[0] - h * (Lwdw01[0][i] + Lwdw01[0][i + 1])
        wi[1] = wi[1] - h * (Lwdw01[1][i] + Lwdw01[1][i + 1])
    else:
        wi[0] = wi[0] - h * (Lwdw01[0][2] + Lwdw01[0][1])
        wi[1] = wi[1] - h * (Lwdw01[1][2] + Lwdw01[1][1])
    return wi


wi = [0, 0]
# Потребовалось около 20 итераций
for j in range(20):
    Lwdw01 = Calc(wi[0], wi[1])
    if j != 0:
        random.shuffle(Lwdw01[0])
        random.shuffle(Lwdw01[1])
    for i in range(2):
        w0w1 = func(wi, i)
        print(wi, j)
