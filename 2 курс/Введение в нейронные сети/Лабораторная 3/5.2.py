class Patient:
    def __init__(self, name, weight, sex, blood, healthy):
        self.name = name
        self.weight = weight
        self.sex = sex
        self.blood = blood
        self.healthy = healthy


def normalize(x, xs, s):
    return (x - xs) / s


A1 = Patient("A1", 50, 0, 1, 0)
A2 = Patient("A2", 60, 1, 2, 1)
A3 = Patient("A3", 80, 1, 3, 0)
A4 = Patient("A4", 100, 0, 4, 1)

patients = [A1, A2, A3, A4]

avg_weight = sum(patient.weight for patient in patients) / len(patients)
std_weight = (sum((patient.weight - avg_weight) ** 2 for patient in patients) / len(patients)) ** 0.5
avg_sex = sum(patient.sex for patient in patients) / len(patients)
std_sex = (sum((patient.sex - avg_sex) ** 2 for patient in patients) / len(patients)) ** 0.5
avg_blood = sum(patient.blood for patient in patients) / len(patients)
std_blood = (sum((patient.blood - avg_blood) ** 2 for patient in patients) / len(patients)) ** 0.5

for patient in patients:
    patient.weight = normalize(patient.weight, avg_weight, std_weight)
    patient.sex = normalize(patient.sex, avg_sex, std_sex)
    patient.blood = normalize(patient.blood, avg_blood, std_blood)

# Нормализация
A = Patient("A", 90, 1, 1, '?')
patients.append(A)
A.weight = normalize(A.weight, avg_weight, std_weight)
A.sex = normalize(A.sex, avg_sex, std_sex)
A.blood = normalize(A.blood, avg_blood, std_blood)

for patient in patients:
    print(', '.join("%s: %s" % item for item in vars(patient).items()))