# x1 = [0, 0, 1, 1]
# x2 = [0, 1, 0, 1]
#  y = [0, 1, 1, 0]
# Fnn = ReLU(ReLU(w11 * x1 + w21 * x2) + ReLU(w12 * x1 + w22 * x2))
# Lw = (Fnn(x1, x2) - 0) ** 2 + (Fnn(x1, x2) - 1) ** 2 + (Fnn(x1, x2) - 1) ** 2 + (Fnn(x1, x2) - 0) ** 2)


def ReLU(x):
    return max(0, x)


def dReLU(x):
    if x > 0:
        return 1
    else:
        return 0


def Fnn(x1, x2, w):
    return ReLU(ReLU(w['11'] * x1 + w['21'] * x2) + ReLU(w['12'] * x1 + w['22'] * x2))


def dFdw(x1, x2, w, i):
    h = 0.01
    w_h = dict(w)
    w_h[i] = w[i] + h
    return (Fnn(x1, x2, w_h) - Fnn(x1, x2, w)) / h


def dLw(wij, w, i):
    return sum([2 * dFdw(wij[0][0], wij[0][1], w, i) * (Fnn(wij[0][0], wij[0][1], w) - wij[0][2]),
                2 * dFdw(wij[1][0], wij[1][1], w, i) * (Fnn(wij[1][0], wij[1][1], w) - wij[1][2]),
                2 * dFdw(wij[2][0], wij[2][1], w, i) * (Fnn(wij[2][0], wij[2][1], w) - wij[2][2]),
                2 * dFdw(wij[3][0], wij[3][1], w, i) * (Fnn(wij[3][0], wij[3][1], w) - wij[3][2])])


def func(wij, w):
    h = 0.1
    w['11'] = w['11'] - h * dLw(wij, w, '11')
    w['12'] = w['12'] - h * dLw(wij, w, '12')
    w['21'] = w['21'] - h * dLw(wij, w, '21')
    w['22'] = w['22'] - h * dLw(wij, w, '22')
    return w


wij = [[0, 0, 0],
       [0, 1, 1],
       [1, 0, 1],
       [1, 1, 2]]


w1 = {'11': 0.5, '12': -0.5, '21': -0.5, '22': 0.5}

for i in range(100):
    func(wij, w1)

print(Fnn(wij[0][0], wij[0][1], w1))
print(Fnn(wij[1][0], wij[1][1], w1))
print(Fnn(wij[2][0], wij[2][1], w1))
print(Fnn(wij[3][0], wij[3][1], w1))
