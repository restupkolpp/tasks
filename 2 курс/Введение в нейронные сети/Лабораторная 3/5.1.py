class Patient:
    def __init__(self, name, weight, sex, blood, healthy):
        self.name = name
        self.weight = weight
        self.sex = sex
        self.blood = blood
        self.healthy = healthy


def normalize(x, a, b):
    return (x - a) / (b - a)


A1 = Patient("A1", 50, 0, 1, 0)
A2 = Patient("A2", 60, 1, 2, 1)
A3 = Patient("A3", 80, 1, 3, 0)
A4 = Patient("A4", 100, 0, 4, 1)

patients = [A1, A2, A3, A4]

min_weight = min(patient.weight for patient in patients)
max_weight = max(patient.weight for patient in patients)
min_sex = min(patient.sex for patient in patients)
max_sex = max(patient.sex for patient in patients)
min_blood = min(patient.blood for patient in patients)
max_blood = max(patient.blood for patient in patients)

for patient in patients:
    patient.weight = normalize(patient.weight, min_weight, max_weight)
    patient.sex = normalize(patient.sex, min_sex, max_sex)
    patient.blood = normalize(patient.blood, min_blood, max_blood)

# Нормализация
A = Patient("A", 90, 1, 1, '?')
patients.append(A)
A.weight = normalize(A.weight, min_weight, max_weight)
A.sex = normalize(A.sex, min_sex, max_sex)
A.blood = normalize(A.blood, min_blood, max_blood)

for patient in patients:
    print(', '.join("%s: %s" % item for item in vars(patient).items()))
