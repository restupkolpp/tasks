from array import *


def func(arr):
    h = 0.1
    w0 = arr[0]
    w1 = arr[1]
    w1 = w1 - h * (4 * w1 + 4)
    w0 = w0 - h * (6 * w0)
    arr[0] = w0
    arr[1] = w1
    return arr


a = [0, 0]

# Примерно на 71 итерации значение почти точно достигает минимума
for i in range(71):
    a = func(a)
    print(a)
