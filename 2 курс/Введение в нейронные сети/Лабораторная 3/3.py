import random
# x = [0, 1, 2]
# y = [1, 2, 3]
# Lw = [((w0 - 1) ** 2), ((w1 + w0 - 2) ** 2), ((2 * w1 + w0 - 3) ** 2)]


def Calc(w0, w1):
    Arr = [0, 0]
    Lwdw1 = [0, (2 * w1 + 2 * w0 - 4), (4 * w1 + 2 * w0 - 6)]
    Lwdw0 = [(2 * w0 - 2), (2 * w1 + 2 * w0 - 4), (4 * w1 + 2 * w0 - 6)]
    Arr[0] = Lwdw0
    Arr[1] = Lwdw1
    return Arr


def func(wi, i):
    global Lwdw01
    h = 0.1
    wi[0] = wi[0] - h * Lwdw01[0][i]
    wi[1] = wi[1] - h * Lwdw01[1][i]
    return wi


wi = [0, 0]
# Потребовалось около 20 итераций
for j in range(20):
    Lwdw01 = Calc(wi[0], wi[1])
    if j != 0:
        random.shuffle(Lwdw01[0])
        random.shuffle(Lwdw01[1])
    for i in range(3):
        w0w1 = func(wi, i)
        print(wi, j)
