package com.example.restik;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class HelloController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button chek;

    @FXML
    private Label countLabel1;

    @FXML
    private Label countLabel2;

    @FXML
    private Label countLabel3;

    @FXML
    private Label finalCount1;

    @FXML
    private Label finalCount2;

    @FXML
    private Label finalCount3;

    @FXML
    private Label finalSumm;

    @FXML
    private Button minus1;

    @FXML
    private Button minus2;

    @FXML
    private Button minus3;

    @FXML
    private Button plus1;

    @FXML
    private Button plus2;

    @FXML
    private Button plus3;

    @FXML
    private Label summ1;

    @FXML
    private Label summ2;

    @FXML
    private Label summ3;

    int countKartoshka = 0;

    @FXML
    private void KartoshkaCountPlus()
    {
        countKartoshka += 1;
        countLabel1.setText(Integer.toString(countKartoshka));
    }

    @FXML
    private void KartoshkaCountMinus() {

        if (countKartoshka > 0)
        {
            countKartoshka -= 1;
            countLabel1.setText(Integer.toString(countKartoshka));
        }
    }

    int countKrylia = 0;

    @FXML
    private void KryliaCountPlus()
    {
        countKrylia += 1;
        countLabel2.setText(Integer.toString(countKrylia));
    }

    @FXML
    private void KryliaCountMinus()
    {
        if(countKrylia > 0)
        {
            countKrylia -= 1;
            countLabel2.setText(Integer.toString(countKrylia));
        }
    }

    int countKokaKola = 0;

    @FXML
    private void KolaCountPlus()
    {
        countKokaKola += 1;
        countLabel3.setText(Integer.toString(countKokaKola));
    }

    @FXML
    private void KolaCountMinus()
    {
        if(countKokaKola > 0)
        {
            countKokaKola -= 1;
            countLabel3.setText(Integer.toString(countKokaKola));
        }
    }

    int finalKartoshka;
    int finalKrylia;
    int finalKokaKola;

    @FXML
    private void FinalChek()
    {
        finalKartoshka = countKartoshka;
        finalKrylia = countKrylia;
        finalKokaKola = countKokaKola;

        finalCount1.setText(countLabel1.getText());
        finalCount2.setText(countLabel2.getText());
        finalCount3.setText(countLabel3.getText());

        summ1.setText(Integer.toString(finalKartoshka * 50));
        summ2.setText(Integer.toString(finalKrylia * 100));
        summ3.setText(Integer.toString(finalKokaKola * 30));

        String s1 = summ1.getText();
        String s2 = summ2.getText();
        String s3 = summ3.getText();

        int summa = Integer.parseInt(s1) + Integer.parseInt(s2) + Integer.parseInt(s3);

        finalSumm.setText(Integer.toString(summa));
    }

    @FXML
    void initialize()
    {

    }

}
