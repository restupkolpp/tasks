module com.example.flag {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.flag to javafx.fxml;
    exports com.example.flag;
}