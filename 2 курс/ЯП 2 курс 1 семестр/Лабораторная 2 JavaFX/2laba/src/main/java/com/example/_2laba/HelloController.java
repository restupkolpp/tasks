package com.example._2laba;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class HelloController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button changeButton;

    @FXML
    private TextField text1;

    @FXML
    private TextField text2;

    @FXML
    private void click()
    {
        if(changeButton.getText().equals("-->"))
        {
            text2.setText(text1.getText());
            text1.setText("");
            changeButton.setText("<--");
        }
        else if (changeButton.getText().equals("<--"))
        {
            text1.setText(text2.getText());
            text2.setText("");
            changeButton.setText("-->");
        }
    }

    @FXML
    void initialize() {
    }

}
