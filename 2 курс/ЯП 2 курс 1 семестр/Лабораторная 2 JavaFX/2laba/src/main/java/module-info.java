module com.example._2laba {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.kordamp.bootstrapfx.core;

    opens com.example._2laba to javafx.fxml;
    exports com.example._2laba;
}