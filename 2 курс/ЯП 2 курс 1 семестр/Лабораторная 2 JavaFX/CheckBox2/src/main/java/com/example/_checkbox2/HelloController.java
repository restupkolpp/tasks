package com.example._checkbox2;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollBar;

public class HelloController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private CheckBox first_checkbox;

    @FXML
    private ScrollBar polzynok;

    @FXML
    private ProgressBar progressbar;

    @FXML
    private RadioButton radio_baton;

    @FXML
    private CheckBox second_checkbox;

    @FXML
    private CheckBox third_checkbox;

    @FXML
    private void method1 ()
    {
        if(first_checkbox.isSelected())
        {
            progressbar.setVisible(false);
        }
        else if (!first_checkbox.isSelected())
        {
            progressbar.setVisible(true);
        }
    }

    @FXML
    private void method2 ()
    {
        if(second_checkbox.isSelected())
        {
            radio_baton.setVisible(false);
        }
        else if (!second_checkbox.isSelected())
        {
            radio_baton.setVisible(true);
        }
    }

    @FXML
    private void method3 ()
    {
        if(third_checkbox.isSelected())
        {
            polzynok.setVisible(false);
        }
        else if (!third_checkbox.isSelected())
        {
            polzynok.setVisible(true);
        }
    }

    @FXML
    void initialize() {

    }


}
