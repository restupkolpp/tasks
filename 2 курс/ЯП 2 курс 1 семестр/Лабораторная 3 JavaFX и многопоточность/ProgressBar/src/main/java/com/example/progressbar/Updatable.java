package com.example.progressbar;

public interface Updatable {
    void update(double value);
}
