package com.example.progressbar;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;

public class HelloController {

    private IModel model;

    @FXML
    private ProgressBar bar;

    @FXML
    private Button pause;

    @FXML
    void PauseResume(ActionEvent event) {
        if(pause.getText().equals("Pause"))
        {
            model.pause();
            pause.setText("Resume");
        }
        else if(pause.getText().equals("Resume"))
        {
            synchronized (System.out)
            {
                System.out.notify();
            }
            model.resume();
            pause.setText("Pause");
        }
    }

    @FXML
    void Start(ActionEvent event) {
        pause.setText("Pause");
        model.some_calc(new Updatable() {
            @Override
            public void update(double value) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        bar.setProgress(value);
                    }
                });
            }
        });
    }

    @FXML
    void Stop(ActionEvent event)
    {
        model.stop();
        bar.setProgress(0);
        pause.setText("Pause");
    }

    @FXML
    void initialize() {
        ModelFactory modelFactory = new ModelFactory();
        model = modelFactory.createInstance();
    }

}
