package com.example.progressbar;

public interface IModel {
    void some_calc(Updatable updater);
    void stop();
    void pause();
    void resume();
    boolean is_alive();
}
