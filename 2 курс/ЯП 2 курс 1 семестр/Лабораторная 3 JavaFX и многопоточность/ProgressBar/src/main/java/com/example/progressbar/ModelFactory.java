package com.example.progressbar;

public class ModelFactory {
    public IModel createInstance() {
        return new Model();
    }
}
