package geometry3d;

import geometry2d.Figure;
import Exceptions.CylinderException;

public class Cylinder 
{
    Figure f;
    double h;
    
    public Cylinder(Figure f1, double h1)
    {
        try 
        {
        if(h1 <= 0)
            throw new CylinderException();
        }
        catch(CylinderException e)
        {
            System.out.println("Exception: " + e.toString());
            return;
        }
        f = f1;
        h = h1;
    }
    
    public double Volume()
    {
        return (f.Area() * h);
    }
}