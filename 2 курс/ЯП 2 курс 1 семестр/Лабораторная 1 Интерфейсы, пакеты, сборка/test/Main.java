package test;

import java.util.Scanner;
import geometry2d.Figure;
import geometry2d.Circle;
import geometry2d.Rectangle;
import geometry3d.Cylinder;

public class Main {
    public static void main(String[] args)
    {
        Figure f;
        Scanner in = new Scanner(System.in);
        System.out.println("Select the cylinder base: ");
        System.out.println("1 - Rectangle ");
        System.out.println("2 - Circle ");
        int i = in.nextInt();
        switch (i) {
            case 1:
                System.out.print("Enter the sides of the rectangle: ");
                int b = in.nextInt();
                int c = in.nextInt();
                f = new Rectangle(b, c);
                break;
            case 2:
                System.out.print("Enter the radius of the circle: ");
                int r = in.nextInt();
                f = new Circle(r);
                break;
            default:
                throw new AssertionError();
        }
        
        System.out.print("Enter the height of the cylinder: ");
        int a = in.nextInt();
        
        Cylinder c = new Cylinder(f, a);
        
        System.out.println(c.Volume());
    }
}