package geometry2d;

import Exceptions.RectangleException;

public class Rectangle implements Figure{
    double a, b;
    
    public Rectangle(double la, double lb)
    {
        try{
            if (la <= 0 || lb <= 0) throw new RectangleException();
        }
        catch(RectangleException e)
        {
            System.out.println("Exception: " + e.toString());
            return;
        }
        a = la;
        b = lb;
    }
    
    public double Area()
    {
        return a * b;
    }
    
    public void Show()
    {
        System.out.print(a + " " + b);
    }
}