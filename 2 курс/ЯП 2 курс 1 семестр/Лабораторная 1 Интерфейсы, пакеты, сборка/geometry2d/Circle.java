package geometry2d;

import Exceptions.CircleException;

public class Circle implements Figure{
    double a = 3;
    
    public Circle(double la)
    {
        try {
            if(la <= 0) throw new CircleException();
        }
        catch(CircleException e)
        {
            System.out.println("Exception: " + e.toString());
            return;
        }
        a=la;
    }    
    public double Area()
    {
        return a * Math.PI;
    }
    
    public void Show()
    {
        System.out.print(a);
    }
}
