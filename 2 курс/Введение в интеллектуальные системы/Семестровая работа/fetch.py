import requests
import log_save as log
from bs4 import BeautifulSoup

headers = {
    'X-API-KEY': 'WQ2PD2K-EXJ4KX7-PJ4H9JC-YDDC704'
}


async def help(message):
    message_text = "Команды бота:\n" \
                   "1. !Rating 'название фильма'- получить рейтинг фильма по названию\n" \
                   "2. !Show 'название фильма' - получить ссылку для просмотра фильма\n" \
                   "3. !Information 'название фильма' - узнать страны производства и жанры фильма\n" \
                   "4. !Poster 'название фильма' - получить постер фильма по его имени\n" \
                   "5. !News - присылает новость с сайта кинопоиск"

    await log.save_to_log(message, message_text)
    await message.reply(message_text)


async def getNews():
    url = 'https://www.kinopoisk.ru/?utm_referrer=yandex.ru'
    response = requests.get(url)
    text = BeautifulSoup(response.text, 'html.parser')
    div = text.find('div', class_="styles_popularPostsColumn___JPK_")
    span = div.find('span', class_="styles_titleWrapper__pjDVp")['href']

    link = f"https://www.kinopoisk.ru {span}"
    return link


async def grads(name, message):
    url = f'https://api.kinopoisk.dev/v1.2/movie/search?page=1&limit=1&query={name}'
    response = requests.get(url, headers=headers)
    if response.json()['docs'][0]['alternativeName'] == '':
        replyText = f"Film {response.json()['docs'][0]['name']} \n" \
                    f"Origin name not found \n" \
                    f"Kinopoisk rating: {response.json()['docs'][0]['rating']}"
    else:
        replyText = f"Film {response.json()['docs'][0]['name']} \n" \
                    f"Origin name: {response.json()['docs'][0]['alternativeName']} \n" \
                    f"Kinopoisk rating: {response.json()['docs'][0]['rating']}"
    await log.save_to_log(message, replyText)
    return replyText


async def watch(name, message):
    url = f'https://api.kinopoisk.dev/v1.2/movie/search?page=1&limit=1&query={name}'
    response = requests.get(url, headers=headers)
    watchUrl = f"https://api.kinopoisk.dev/v1.3/movie/{response.json()['docs'][0]['id']}"
    watchResponse = requests.get(watchUrl, headers=headers)
    if watchResponse.json()['watchability']['items'][0]['url'] == '':
        replyText = f"Name of film: {watchResponse.json()['name']} \n" \
                    f"Site link not found"
    else:
        replyText = f"Name of film: {watchResponse.json()['name']} \n" \
                f"Site link: {watchResponse.json()['watchability']['items'][0]['url']}"
    await log.save_to_log(message, replyText)
    return replyText


async def info(name, message):
    url = f'https://api.kinopoisk.dev/v1.2/movie/search?page=1&limit=1&query={name}'
    response = requests.get(url, headers=headers)
    genres = response.json()['docs'][0]['genres']
    countries = response.json()['docs'][0]['countries']
    a = []
    b = []
    for i in range(len(genres)):
        a.append(genres[i])
    for i in range(len(countries)):
        b.append(countries[i])
    replyText = f"Film: {response.json()['docs'][0]['name']} \n" \
                f"Genres: {a} \n" \
                f"Countries: {b}"
    await log.save_to_log(message, replyText)
    return replyText


async def getPoster(name, message):
    url = f'https://api.kinopoisk.dev/v1.2/movie/search?page=1&limit=1&query={name}'
    response = requests.get(url, headers=headers)
    print(response.json())
    watchUrl = f"https://api.kinopoisk.dev/v1.3/movie/{response.json()['docs'][0]['id']}"
    watchResponse = requests.get(watchUrl, headers=headers)
    if watchResponse.json()['poster']['url'] == '':
        replyText = 'Poster not found'
    else:
        replyText = watchResponse.json()['poster']['url']
    await log.save_to_log(message, replyText)
    return replyText
