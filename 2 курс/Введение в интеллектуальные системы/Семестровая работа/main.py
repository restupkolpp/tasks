import discord
import asyncio
import log_save as log
from fetch import grads
from fetch import watch
from fetch import info
from discord.ext import commands
from fetch import help
from fetch import getNews
from fetch import getPoster

prefix = '!'
D_TOKEN = "MTEwOTcxNDQzMjE1MTUzOTc1Mw.G0sI3L.txNFB90580kQSe7OPP9CWufOPQEYwpm7ToAfVE"

intents = discord.Intents.default()
intents.message_content = True

bot = commands.Bot(intents=intents, command_prefix=prefix)


@bot.event
async def on_ready(): print('Bot Online!')


@bot.event
async def on_message(message):
    if message.author == bot.user:
        return
    if message.content.startswith(prefix):
        if message.content.startswith('!help'):
            await help(message)

        elif message.content.startswith('!Rating'):
            filmName = message.content
            filmName = filmName[7:]
            filmName = filmName.lstrip().rstrip()
            response = await grads(filmName, message)
            await message.reply(response)

        elif message.content.startswith('!Show'):
            filmName = message.content
            filmName = filmName[5:]
            filmName = filmName.lstrip().rstrip()
            response = await watch(filmName, message)
            await message.reply(response)

        elif message.content.startswith('!Information'):
            filmName = message.content
            filmName = filmName[12:]
            filmName = filmName.lstrip().rstrip()
            response = await info(filmName, message)
            await message.reply(response)

        elif message.content.startswith('!Poster'):
            filmName = message.content
            filmName = filmName[7:]
            filmName = filmName.lstrip().rstrip()
            response = await getPoster(filmName, message)
            await message.reply(response)
        elif message.content.startswith('!News'):
            response = await getNews()
            await message.reply(response)
        else:
            await message.reply("Пожалуйста, введите одну из команд")
    else:
        await log.save_to_log(message, 'None')
        return


intents = discord.Intents.default()
intents.members = True
intents.message_content = True

bot.run(D_TOKEN)
