async def save_to_log(message, bot_reply):
    file_name = './log/' + str(message.author.id) + '.log'
    file = open(file_name, 'a')
    log_content = "ID=" + str(message.author.id) + "\nMSG=" + message.content + "\nBOT_REPLY=" + bot_reply + "\n"
    log_content = log_content.encode('utf-8')
    file.write(str(log_content))
    file.close()
