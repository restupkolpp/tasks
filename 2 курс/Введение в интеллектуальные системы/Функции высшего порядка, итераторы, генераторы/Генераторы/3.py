def square_fibonacci(n):
    a, b = 1, 1
    for p in range(n):
        yield a ** 2
        a, b = b, a + b


i = int(input())
c = square_fibonacci(i)

print(next(c))
print(next(c))
print(next(c))
print(next(c))
print(next(c))
