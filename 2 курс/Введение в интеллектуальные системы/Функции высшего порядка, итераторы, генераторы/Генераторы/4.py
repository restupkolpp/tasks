def alfavit (n):
    a = ord('а')
    for p in range(a, a + 32):
        if p == ord('ж'):
            yield chr(a + 33)
        yield chr(p)


c = alfavit(34)
for i in c:
    print(i)
