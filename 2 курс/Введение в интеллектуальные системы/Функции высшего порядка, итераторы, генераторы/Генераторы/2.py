import math


def factorials(n):
    for j in range(1, n + 1):
        yield math.factorial(j)


i = int(input())
a = factorials(i)

print(next(a))
print(next(a))
print(next(a))
print(next(a))
print(next(a))
print(next(a))
