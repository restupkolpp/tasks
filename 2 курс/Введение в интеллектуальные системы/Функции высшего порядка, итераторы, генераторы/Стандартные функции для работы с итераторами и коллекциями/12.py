import math

values = [[2, 0], [3, 4], [20, -4], [3, -4], [-3, 4]]
print(*sorted(values, key=lambda x: [math.sqrt(pow(x[0], 2) + pow(x[1], 2)), x]))
