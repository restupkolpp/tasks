from math import gcd
from functools import reduce

print(reduce(gcd, [36, 12, 144, 18]))
