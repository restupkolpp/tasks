table = "10 9 8 7\n6 5 4 3\n2 1 0 -1\n-2 -3 -4 -5"

print(any(not all(map(int, i.split())) for i in table.split("\n")))
