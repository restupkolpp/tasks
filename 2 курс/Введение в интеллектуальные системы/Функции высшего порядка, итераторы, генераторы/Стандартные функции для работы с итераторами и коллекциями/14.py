import sys

arr = list(enumerate([i.split()[0] for i in sys.stdin.read().split()]))
res = [i for i in arr if i[1][0].isupper()]

for i in range(len(res)):
    print(f"{res[i][0]} - {res[i][1]}")
