def cached(func):
    cache = {}

    def wrapper(n):
        nonlocal cache
        print(cache)
        if n in cache.keys():
            return cache[n]
        cache[n] = func(n)
        return cache[n]
    return wrapper


@cached
def fib(n):
    if n == 1 or n == 2:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)


print(fib(int(input("Введите число.\n"))))
