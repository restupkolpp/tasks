def check_password(password):
    def burger_decorator(func):
        def wrapper(*args, **kwargs):
            if len(password) > 6 and not password.isdigit():
                func(*args, **kwargs)
            else:
                print("В доступе отказано.")
                return None
        return wrapper
    return burger_decorator


@check_password('1234567f')
def make_burger(typeOfMeat, withOnion=False, withTomato=True):
    print(f"burger with {typeOfMeat}")


make_burger("pig")
