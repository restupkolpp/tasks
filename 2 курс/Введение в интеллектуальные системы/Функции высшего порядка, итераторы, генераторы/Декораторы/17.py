def check_password(func):
    def wrapper(*args, **kwargs):
        password = input("Введите пароль.\n")
        if len(password) > 6 and not password.isdigit():
            return func(*args, **kwargs)
        else:
            print("В доступе отказано.")
            return None
    return wrapper


@check_password
def login():
    print("Вход выполнен успешно!")


login()
