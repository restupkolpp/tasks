gl = ['a', 'e', 'i', 'o', 'u', 'y']
sogl = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm',
        'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z']


def ask_password(login, password, success, failure):
    gl_passwd = list(map(lambda x: x.lower(),
                         filter(lambda x: x.lower() in gl, password)))
    sogl_passwd = list(map(lambda x: x.lower(),
                           filter(lambda x: x.lower() in sogl, password)))
    sogl_login = list(map(lambda x: x.lower(),
                          filter(lambda x: x.lower() in sogl, login)))
    if len(gl_passwd) == 3 and sogl_passwd == sogl_login:
        success(login)
        return True, ''
    elif len(gl_passwd) != 3 and sogl_passwd == sogl_login:
        failure(login, "Wrong number of vowels")
        return False, 'Wrong number of vowels'
    elif len(gl_passwd) == 3 and sogl_passwd != sogl_login:
        failure(login, "Wrong consonants")
        return False, 'Wrong consonants'
    elif len(gl_passwd) != 3 and sogl_passwd != sogl_login:
        failure(login, "Everything is wrong")
        return False, 'Everything is wrong'


def success(login):
    return


def failure(login, message):
    return


def main(login, password):
    e, mes = ask_password(login, password, success, failure)
    if e:
        print('Привет, ' + login.lower() + '!')
    else:
        print('Кто-то пытался притвориться пользователем ' + login.lower() + ', но в пароле'
              'допустил ошибку: ' + mes.upper() + '.')


main("anastasia", "nsyatos")
main("eugene", "aanig")
ask_password("anastasia", "nsyatos", lambda login: print('super'), lambda login, err: print('bad'))

