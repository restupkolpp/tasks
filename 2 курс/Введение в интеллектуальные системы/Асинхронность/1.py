import asyncio
import os
from random import randint


async def factorial(name, number):
    f = 1
    for i in range(2, number + 1):
        print(f"Task {name}: Compute factorial({i})...")
        f *= i
        await asyncio.sleep(0.01 * randint(1, 10))
    print(f"Task {name}: factorial({number}) = {f}")


async def main():
    tasks = [
        asyncio.create_task(factorial("A", 15)),
        asyncio.create_task(factorial("B", 7)),
        asyncio.create_task(factorial("C", 4)),
    ]
    await asyncio.gather(*tasks)


if __name__ == '__main__':
    if os.name == 'nt':
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(main())
