import discord
import aiohttp
D_TOKEN = "MTEwOTcxNDQzMjE1MTUzOTc1Mw.G0sI3L.txNFB90580kQSe7OPP9CWufOPQEYwpm7ToAfVE"


class Test(discord.Client):
    async def on_ready(self):
        print('Готов показать пёсика или котика!')

    async def on_message(self, message):
        if message.author == self.user:
            return
        
        if 'пёсик!' in message.content.lower():
            async with aiohttp.ClientSession() as session:
                async with session.get('https://dog.ceo/api/breeds/image/random') as resp:
                    data = await resp.json()
            if data['status'] == 'success':
                await message.channel.send(data['message'])

        elif 'котик!' in message.content.lower():
            async with aiohttp.ClientSession() as session:
                async with session.get('https://meow.senither.com/v1/random') as resp:
                    data = await resp.json()
            if data['status'] == 200:
                await message.channel.send(data['data']['url'])


intents = discord.Intents.default()
intents.members = True
intents.message_content = True
client = Test(intents=intents)
client.run(D_TOKEN)
