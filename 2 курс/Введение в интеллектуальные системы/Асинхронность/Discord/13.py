import asyncio
import discord

D_TOKEN = "MTEwOTcxNDQzMjE1MTUzOTc1Mw.G0sI3L.txNFB90580kQSe7OPP9CWufOPQEYwpm7ToAfVE"


class Test(discord.Client):
    async def on_ready(self):
        print('Готов!')

    async def on_message(self, message):
        args = []
        if message.author == self.user:
            return
        if 'set_timer' in message.content.lower():
            args = [int(message.content.lower().split()[i]) for i in range(1, 4)]
            await asyncio.sleep(args[0] * 3600 + args[1] * 60 + args[2])
            await message.channel.send('Время X наступило!')


intents = discord.Intents.default()
intents.members = True
intents.message_content = True
client = Test(intents=intents)
client.run(D_TOKEN)
