from telegram.ext import Application, CommandHandler
from datetime import datetime, date
TG_TOKEN = "6213381241:AAHNJkFqw3vSVIUOPnzsu2iE1p8QPrMZPOU"


async def get_date(update, context):
    await update.message.reply_text(f'Дата: {date.today().strftime("%d.%m.%Y")}')


async def get_time(update, context):
    await update.message.reply_text(f'Время: {datetime.now().strftime("%H:%M:%S ")}')


def main():
    application = Application.builder().token(TG_TOKEN).build()

    application.add_handler(CommandHandler('date', get_date))
    application.add_handler(CommandHandler('time', get_time))

    application.run_polling()


if __name__ == '__main__':
    main()
