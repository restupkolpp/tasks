from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import Application, CommandHandler, ConversationHandler, MessageHandler, filters
TG_TOKEN = "6213381241:AAHNJkFqw3vSVIUOPnzsu2iE1p8QPrMZPOU"

enter = [['1']]
room1 = [['2', '/exit']]
room2 = [['3']]
room3 = [['1', '4']]
room4 = [['1']]

enter_markup = ReplyKeyboardMarkup(enter, one_time_keyboard=False)
room1_markup = ReplyKeyboardMarkup(room1, one_time_keyboard=False)
room2_markup = ReplyKeyboardMarkup(room2, one_time_keyboard=False)
room3_markup = ReplyKeyboardMarkup(room3, one_time_keyboard=False)
room4_markup = ReplyKeyboardMarkup(room4, one_time_keyboard=False)


async def start(update, context):
    await update.message.reply_text("Добро пожаловать! Пожалуйста, сдайте верхнюю одежду в гардероб!",
                                    reply_markup=ReplyKeyboardRemove())
    await update.message.reply_text("Вы можете пройти в 1 зал.",
                                    reply_markup=enter_markup)
    return 1


async def exit_museum(update, context):
    if context.user_data['can_exit']:
        await update.message.reply_text("Всего доброго, не забудьте забрать верхнюю одежду в гардеробе!",
                                        reply_markup=ReplyKeyboardRemove())
        return ConversationHandler.END
    else:
        await update.message.reply_text("Вы не можете выйти из музея из этого зала, пройдите в зал 1.")


async def first_room(update, context):
    context.user_data['can_exit'] = True

    if update.message.text == '1':
        await update.message.reply_text("Вы находитесь в 1 зале, от сюда вы можете пройти во 2 зал или выйти из музея",
                                        reply_markup=room1_markup)
    elif update.message.text == 'выход':
        await exit_museum(update, context)
    return 2


async def second_room(update, context):
    context.user_data['can_exit'] = False
    await update.message.reply_text("Сейчас вы находитесь в 2 зале, от сюда вы можете перейти в помещение 3.",
                                    reply_markup=room2_markup)
    return 3


async def third_room(update, context):
    await update.message.reply_text("Сейчас вы находитесь в 3 зале, от сюда вы можете перейти в помещение 4 и 1.",
                                    reply_markup=room3_markup)
    return 4


async def fourth_room(update, context):
    if update.message.text == '1':
        await first_room(update, context)
        return 2
    elif update.message.text == '4':
        await update.message.reply_text("Сейчас вы находитесь в 4 зале, от сюда вы можете перейти в помещение 1.",
                                        reply_markup=room4_markup)
        return 1


def main():
    application = Application.builder().token(TG_TOKEN).build()
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            1: [MessageHandler(filters.TEXT & ~filters.COMMAND, first_room)],
            2: [MessageHandler(filters.TEXT & ~filters.COMMAND, second_room)],
            3: [MessageHandler(filters.TEXT & ~filters.COMMAND, third_room)],
            4: [MessageHandler(filters.TEXT & ~filters.COMMAND, fourth_room)]
        },
        fallbacks=[CommandHandler('exit', exit_museum)]
    )
    application.add_handler(conv_handler)
    application.run_polling()


if __name__ == '__main__':
    main()
