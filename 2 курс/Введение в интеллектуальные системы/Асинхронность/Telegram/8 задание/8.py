from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import Application, CommandHandler, ConversationHandler, MessageHandler, filters
import json
import random
TG_TOKEN = "6213381241:AAHNJkFqw3vSVIUOPnzsu2iE1p8QPrMZPOU"

questions = [['Да', '/exit']]
questions_marcup = ReplyKeyboardMarkup(questions, one_time_keyboard=False)


async def start(update, context):
    with open('files/test.json', encoding='utf8') as file:
        test = json.load(file)
        context.user_data['questions'] = [[test['test'][i]['question'],
                                           test['test'][i]['response']]for i in range(0, 10)]
        context.user_data['right_answer'] = 0
        context.user_data['first_start'] = True
    await update.message.reply_text("Не хотите ли пройти опрос?", reply_markup=questions_marcup)
    return 1


async def stop_func(update, context):
    await update.message.reply_text("Было приятно с вами общаться!", reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


async def answers(update, context):
    if not context.user_data['first_start']:
        if update.message.text.lower() == context.user_data['questions'][context.user_data['random']][1]:
            context.user_data['right_answer'] += 1
        context.user_data['questions'].pop(context.user_data['random'])
    context.user_data['first_start'] = False

    if context.user_data['questions']:
        context.user_data['random'] = random.randint(0, len(context.user_data['questions']) - 1)
        await update.message.reply_text(context.user_data['questions'][context.user_data['random']][0],
                                        reply_markup=ReplyKeyboardRemove())
        return 1

    await update.message.reply_text(f"Количество правильных ответов: {context.user_data['right_answer']}")
    return ConversationHandler.END


def main():
    application = Application.builder().token(TG_TOKEN).build()

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            1: [MessageHandler(filters.TEXT & ~filters.COMMAND, answers)]
        },
        fallbacks=[CommandHandler('exit', stop_func)]
    )

    application.add_handler(conv_handler)
    application.run_polling()


if __name__ == '__main__':
    main()
