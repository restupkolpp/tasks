import requests
import asyncio
import os


async def get_address(URL, field):
    result = requests.get(URL).json()
    return URL, result[field[URL]]


async def main():
    list = ['http://ip-api.com/json/', 'https://api.ipify.org/?format=json']
    slovar = {
        'https://api.ipify.org/?format=json': 'ip',
        'http://ip-api.com/json/': 'query'
    }

    tasks = [get_address(item, slovar) for item in list]

    for future in asyncio.as_completed(tasks):
        url, ip = await future
        print(f"Site: {url}: {ip}")
        break


if __name__ == '__main__':
    if os.name == 'nt':
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(main())

