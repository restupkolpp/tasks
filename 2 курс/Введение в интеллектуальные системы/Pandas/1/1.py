import numpy as np
import pandas as pd

# 1
s = pd.Series(data=np.arange(1, 6), index=['a', 'b', 'c', 'd', 'e'])
print('Task 1')
print(s, '\n')

# 2
print('Task 2')
print(s['d'], '\n')

# 3
print('Task 3')
print(s[1], '\n')

# 4
print('Task 4')
s['f'] = 6
print(s, '\n')

# 5
print('Task 5')
print(s[2:5], '\n')

# 6
print('Task 6')
a = {'col1': [1, 5, 3.7], 'col2': [2, 3, 4.8]}
d = pd.DataFrame(a)
print(d, '\n')

# 7
print('Task 7')
print(d['col1'][2], '\n')

# 8
print('Task 8')
d['col2'][1] = 9
print(d, '\n')

# 9
print('Task 9')
print(d[1:], '\n')

# 10
print('Task 10')
d['col3'] = d['col1'] * d['col2']
print(d, '\n')
