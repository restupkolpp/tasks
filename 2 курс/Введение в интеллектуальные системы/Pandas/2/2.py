import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

data = [["Вжик", "Zipper the Fly", "fly", "0.7"],
        ["Гайка", "Gadget Hackwrench", "mouse", None],
        ["Дейл", "Dale", "chipmunk", "1"],
        ["Рокфор", "Monterey Jack", "mouse", "0.8"],
        ["Чип", "Chip", "chipmunk", "0.2"]]

# 1
print('Task1')
df = pd.DataFrame(data, columns=['ru_name', 'en_name', 'class', 'cheer'])
df['cheer'] = df['cheer'].astype('float')
print(df, '\n')
print(df['cheer'], '\n')

# 2
print('Task2')
print(len(df), '\n')

# 3
print('Task3')
print(df.cheer.count(), '\n')

# 4
print('Task4')
print(df.cheer.count(), '\n')

# 5
print('Task5')
dfl = df[1:4].iloc[:, :3]
print(dfl, '\n')

# 6
print('Task6')
df.columns = ['ru_name', 'en_name', 'class', 'cheer']
print(df, '\n')

# 7
print('Task7')
df['logcheer'] = np.log(df['cheer'])
print(df['logcheer'], '\n')

# 8
print('Task8')
x = df['class'].unique()
y = df['class'].value_counts()
dfb = pd.DataFrame({'counts': y}, index=x)
dfb.plot(kind='bar')
plt.xlabel('Class names')
plt.ylabel('Frequency')
plt.title('Name')
print(dfb)
