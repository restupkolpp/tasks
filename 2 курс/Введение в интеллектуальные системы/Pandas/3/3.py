import numpy as np
import pandas as pd

# 1
print('Task1')
df = pd.read_csv('file/wr88125 (1).txt', sep=';')
df.columns = ['index', 'year', 'month', 'day', 'min_t', 'average_t', 'max_t', 'rainfall']
for i in df.columns[4:]:
    df[i] = pd.to_numeric(df[i], errors='coerce')
print(df, '\n')

# 2
print('Task2')
df.pop('index')
print(df, '\n')

# 5
print('Task5')
df['date'] = pd.to_datetime(df[['year', 'month', 'day']])
df = df.drop(['year', 'month', 'day'], axis=1)
print(df, '\n')

# 6
print('Task6')
lst = []
days_count = 0
for i in df.index:
    if i != df.index[0] and df['rainfall'][i - 1] == 0:
        days_count += 1
    else:
        days_count = 0
    lst.append([df['max_t'][i] - df['min_t'][i], days_count])
df['temp'] = [lst[i][0] for i in range(0, len(lst))]
df['days_count'] = [lst[i][1] for i in range(0, len(lst))]
print(df, '\n')

# 7
print('Task7')
print(np.max(df['days_count']), '\n')

# 9
print('Task9')
print(df[df['average_t'] < -30], '\n')
print(df[(df['average_t'] > 27) & (df['days_count'] > 3)], '\n')
