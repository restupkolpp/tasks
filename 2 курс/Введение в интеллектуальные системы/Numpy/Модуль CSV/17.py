import csv

with open('./files/17.csv', newline='') as file:
    reader = csv.DictReader(file, dialect=csv.Sniffer().sniff(file.read()))
    file.seek(0)
    for row in reader:
        if int(row['Old price']) > int(row['New price']):
            print(row['Name'])
