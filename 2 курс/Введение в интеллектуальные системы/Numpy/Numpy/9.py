import numpy as np

x = np.array([-3, 4, 1, 4, 3, 1]).reshape(2, 3)
y = np.array([10, 12]).reshape(2, 1)
i = np.eye(3)
l = 0.1

print(np.linalg.inv(x.transpose() @ x + l * i) @ x.transpose() @ y)