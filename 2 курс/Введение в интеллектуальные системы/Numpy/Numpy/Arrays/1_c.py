import numpy as np

arr_1 = np.full((3, 4), 3)
print(arr_1)
print(np.size(arr_1))

print()
arr_2 = np.random.randint(0, 10, (2, 4))
print(arr_2)
print(np.size(arr_2))
