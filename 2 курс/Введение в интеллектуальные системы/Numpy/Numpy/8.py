import numpy as np


def func(m, n):
    a = np.random.randint(1, 10, (n, m))
    b = np.random.randint(1, 10, (1, m))
    c = np.random.randint(1, 10, (m, n))
    d = np.random.randint(1, 10, (1, m))
    return 5 * a * b + c.transpose() * d


print(func(10, 10))
