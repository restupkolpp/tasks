import numpy as np


def solve(a, b):
    if np.linalg.det(a) == 0:
        return "Система не имеет решения, либо имеет их бесконечно много."
    return np.linalg.solve(a, b)


a = []
b = []
a.append(int(input("Введите значение а11:")))
a.append(int(input("Введите значение а12:")))
a.append(int(input("Введите значение а21:")))
a.append(int(input("Введите значение а22:")))
b.append(int(input("Введите значение b1:")))
b.append(int(input("Введите значение b2:")))
print(solve(np.array(a).reshape(2, 2), np.array(b)))
