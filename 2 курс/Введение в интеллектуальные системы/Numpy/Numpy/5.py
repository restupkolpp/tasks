import numpy as np
import time


def mult(arr1, arr2):
    res = [[0 for i in range(len(arr1))] for i in range(len(arr1))]
    for i in range(len(arr1)):
        for j in range(len(arr1)):
            for k in range(len(arr1)):
                res[i][j] += arr1[i][k] * arr2[k][j]
    return np.array(res)


def el_mult(arr1, arr2):
    res = [[0 for i in range(len(arr1))] for i in range(len(arr1))]
    for i in range(len(arr1)):
        for j in range(len(arr1)):
            res[i][j] += arr1[i][j] * arr2[i][j]
    return np.array(res)


def np_mult(arr1, arr2):
    return arr1 @ arr2


def np_el_mult(arr1, arr2):
    return arr1 * arr2


a = np.random.randint(0, 10, (500, 500))
b = np.random.randint(0, 10, (500, 500))

start_time = time.time()
mult(a, b)
print("mult:", (time.time() - start_time))

start_time = time.time()
el_mult(a, b)
print("el_mult:", (time.time() - start_time))

start_time = time.time()
np_mult(a, b)
print("np_mult:", (time.time() - start_time))

start_time = time.time()
np_el_mult(a, b)
print("np_el_mult:", (time.time() - start_time))
