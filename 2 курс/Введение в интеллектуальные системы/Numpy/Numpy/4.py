import numpy as np


arr = np.arange(-2 * np.pi, 2 * np.pi)
print(arr)
print(np.all((np.sin(arr) ** 2 + np.cos(arr) ** 2).round() == 1))
