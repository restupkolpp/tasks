import numpy as np
from PIL import Image


def bw_convert(p):
    image = np.asarray(Image.open(p))
    x, y, z = image.shape
    consts = np.array([[[0.2989, 0.587, 0.114]]])
    image2 = np.round(np.sum(image * consts, axis=2)).astype(np.uint8).reshape(x, y)

    Image.fromarray(image2).save('./files/1_1.jpg')


bw_convert('./files/1.jpg')
