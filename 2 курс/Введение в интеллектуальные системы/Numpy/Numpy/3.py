import numpy as np


def super_sort(rows, cols):
    a = np.random.randint(1, 101, (rows, cols))
    b = a.copy()
    b[:, ::2] = np.sort(b[:, ::2], axis=0)
    b[:, 1::2] = -np.sort(-b[:, 1::2], axis=0)
    return np.ravel(np.concatenate([a, b], axis=0))


print(super_sort(4, 5))
