actions = {'write': 'W', 'read': 'R', 'execute': 'X'}
file_permissions = {}
for i in range(int(input())):
    name, *permissions = input().split()
    file_permissions[name] = set(permissions)

for i in range(int(input())):
    action, name = input().split()
    if actions[action] in file_permissions[name]:
        print("OK")
    else:
        print("Access denied")
