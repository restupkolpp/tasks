x = int(input())
same = set()
notSame = set()
count = 0
for i in range(x):
    s = input()
    if s in same:
        notSame.add(s)
        count += 1
    else:
        same.add(s)
count += len(notSame)
print(count)
