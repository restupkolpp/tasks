a = []
for i in range(int(input())):
    k = input()
    if k.find('Кто последний?') != -1:
        a.append(k[k.find('Я - ') + 4:-1])
    elif k.find('Я только спросить!') != -1:
        a.insert(0, k[k.find('Я - ') + 4:-1])
    else:
        try:
            print(f'Заходит {a.pop(0)}!')
        except BaseException:
            print('В очереди никого нет')
