s = input()
if len(s) % 2 == 0:
    n = len(s) // 2
    for i in range(n):
        print(' ' * (n - i - 1) + s[n - i - 1] + ' ' * i + ' ' * i + s[n + i])
else:
    n = (len(s) + 1) // 2
    print(' ' * ((len(s) - 1) // 2) + s[n - 1] + ' ' * ((len(s) - 1) // 2))
    for i in range(1, n):
        print(' ' * (n - i - 1) + s[n - i - 1] + ' ' * i + ' ' * (i - 1) + s[n + i - 1])
