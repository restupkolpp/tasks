n = 8
x = []
y = []
for i in range(n):
    x1 = [2, 3, 4, 6, 4, 3, 8, 2]
    y1 = [3, 5, 7, 6, 4, 3, 8, 1]
    x.append(x1)
    y.append(y1)

c = True
for i in range(n):
    for j in range(i + 1, n):
        if x[i] == x[j] or y[i] == y[j] or abs(x[i] - x[j]) == abs(y[i] - y[j]):
            c = False

if c:
    print('NO')
else:
    print('YES')
