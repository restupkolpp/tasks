def transpose(matrix):
    res = []
    n = len(matrix)
    m = len(matrix[0])
    for i in range(n):
        tmp = []
        for j in range(m):
            tmp += [matrix[i][j]]
        res += [tmp]
    return res


matrix = [[1, 2], [3, 4]]
transpose(matrix)
for line in matrix:
    print(*line)
