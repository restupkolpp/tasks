import re


def palindrome(s):
    s1 = s.upper()
    reverse = re.sub(r"\s1+", "", s1)
    r = ''.join(reversed(reverse))
    if s1 == reverse:
        return "Палиндром"
    else:
        return "Не палиндром"


print(palindrome("А роза упала на лапу Азора"))
