import requests
import json

geocoder_request = "http://geocode-maps.yandex.ru/1.x/?apikey=77ade68f-e067-49cc-803c-e7108c8cdc50&geocode=ул. Петровка, 38, " \
                   "стр. 1, Москва&format=json"

response = requests.get(geocoder_request)

data = response.json()

if response:
    print("Почтовый индекс:", data['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']
                                  ['metaDataProperty']['GeocoderMetaData']['Address']['postal_code'])
else:
    print("Ошибка выполнения запроса:")
    print(geocoder_request)
    print("Http статус:", response.status_code, "(", response.reason, ")")
