import requests
import json


def most_south(cities):
    res = []
    for city in cities:
        request = f"http://geocode-maps.yandex.ru/1.x/?apikey=77ade68f-e067-49cc-803c-e7108c8cdc50&geocode={city}&format=json"
        response = requests.get(request)
        data = json.loads(response.content)

        if response:
            coord = data['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'].split(' ')[1]
            res.append([city, float(coord)])
        else:
            print("Ошибка выполнения запроса")
            print("Http статус:", response.status_code, "(", response.reason, ")")

    res.sort(key=lambda x: -x[1])
    print(res[-1][0])


cities_input = input('Введите список городов через запятую\n')
cities = cities_input.split(',')
most_south(cities)
