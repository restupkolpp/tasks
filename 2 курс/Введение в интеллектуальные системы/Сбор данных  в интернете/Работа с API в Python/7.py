import requests

request = "https://static-maps.yandex.ru/1.x/?ll=86.086848,55.355200&size=300,300&z=11&l=map&pt=86.059819,55.344182," \
          "pm2dgm1~86.125121,55.388747,pm2rdm2~86.071734,55.375453,pm2blm3~86.128287,55.342478,pm2orm4"
response = requests.get(request)

if response:
    with open('./files/7.png', 'wb') as file:
        file.write(response.content)
else:
    print("Ошибка выполнения запроса:")
    print(request)
    print("Http статус:", response.status_code, "(", response.reason, ")")
