import requests
import json

barnaul_request = "https://geocode-maps.yandex.ru/1.x/?format=json&apikey=77ade68f-e067-49cc-803c-e7108c8cdc50" \
                   "&geocode=г.Барнаул"

meleuz_request = "https://geocode-maps.yandex.ru/1.x/?format=json&apikey=77ade68f-e067-49cc-803c-e7108c8cdc50&geocode" \
                 "=г.Мелеуз"

ioshkar_ola_request = "https://geocode-maps.yandex.ru/1.x/?format=json&apikey=77ade68f-e067-49cc-803c-e7108c8cdc50" \
                      "&geocode=г.Йошкар-ола"

barnaul = json.loads(requests.get(barnaul_request).content)
meleuz = json.loads(requests.get(meleuz_request).content)
ioshkar_ola = json.loads(requests.get(ioshkar_ola_request).content)

barnaul_data = barnaul['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['Address']['Components'][2]['name']
meleuz_data = meleuz['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['Address']['Components'][2]['name']
ioshkar_ola_data = ioshkar_ola['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['Address']['Components'][2]['name']

print("Барнаул:", barnaul_data)
print("Мелеуз:", meleuz_data)
print("Йошкар-Ола:", ioshkar_ola_data)
