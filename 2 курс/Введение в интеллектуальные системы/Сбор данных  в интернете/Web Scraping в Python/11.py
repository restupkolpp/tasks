from bs4 import BeautifulSoup
import requests

r = requests.get('http://olympus.realpython.org/profiles')
html_text = r.text

soup = BeautifulSoup(html_text, "html.parser")

links = [link.get("href") for link in soup.find_all("a")]

for link in links:
    print("http://olympus.realpython.org" + link)
