class Polynomial:
    def __init__(self, k):
        self.k = k

    def __call__(self, x):
        s = 0
        for i in range(len(self.k)):
            s += self.k[i] * pow(x, i)
        return s

    def __add__(self, other):
        st = []
        ko = Polynomial(st)
        if len(self.k) < len(other.k):
            m = len(self.k)
        else:
            m = len(other.k)
        for i in range(m):
            st.append(self.k[i] + other.k[i])
        if len(self.k) > m:
            st += self.k[m::]
        else:
            st += other.k[m::]
        ko.k = st
        return ko


poly = Polynomial([0, 0, 1])
print(poly(-2))
print(poly(-1))
print(poly(0))
print(poly(1))
print(poly(2))
print()

poly1 = Polynomial([0, 0, 2])
print(poly1(-2))
print(poly1(-1))
print(poly1(0))
print(poly1(1))
print(poly1(2))
print()

poly2 = poly + poly1
print(poly2(-2))
print(poly2(-1))
print(poly2(0))
print(poly2(1))
print(poly2(2))
print()
