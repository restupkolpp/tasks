class FileSystem:
    def __init__(self):
        self.dir_list = []
        self.file_list = []

    def create_dir(self, dir_path):
        temp = dir_path.split("/")
        if temp[0] in [self.dir_list[i].name for i in range(len(self.dir_list))]:
            print("Same directory already exists")
            raise ValueError
        else:
            self.dir_list.append(Directory(temp[0]))
        for i in range(1, len(temp)):
            self.dir_list[-1].add(Directory(temp[i]))
        self.dir_list.sort(key=lambda x: isinstance(x, Directory), reverse=True)

    def list(self, dir_path):
        res = ""
        if dir_path == "/":
            for i in self.dir_list:
                if isinstance(i, Directory):
                    res += i.name + "/"
                elif isinstance(i, File):
                    res += i.name + "." + i.type
                res += "\n"
            return res
        else:
            return self.goto(dir_path).list()

    def create_file(self, path, file):
        if path == "/" and file not in self.file_list:
            self.file_list.append(file)
        else:
            self.goto(path).add(file)

    def read_file(self, path, filename):
        if path == "/":
            for i in self.file_list:
                if filename == i.name:
                    return i.read()
        else:
            return self.goto(path).read(filename)

    def goto(self, path):
        temp = 0
        for i in self.dir_list:
            if i.name == path.split("/")[0]:
                temp = i
        if temp != 0:
            return temp.goto("/".join(path.split("/")[1:]))
        else:
            return "There is no such directory!"


class Directory:
    def __init__(self, name):
        self.lst = []
        self.name = name

    def add(self, file):
        if file in self.lst:
            print("Same file already exists")
        elif isinstance(file, Directory) and file.name in [self.lst[i].name for i in range(len(self.lst))
                                                           if isinstance(self.lst[i], Directory)]:
            print("Same directory already exists")
        else:
            self.lst.append(file)
            self.lst.sort(key=lambda x: isinstance(x, Directory), reverse=True)

    def list(self):
        res = ""
        for i in self.lst:
            if isinstance(i, Directory):
                res += i.name + "/"
            elif isinstance(i, File):
                res += i.name + "." + i.type
            res += "\n"
        return res

    def goto(self, path):
        if path == "" or path == self.name:
            return self
        else:
            temp = 0
            for i in self.lst:
                if i.name == path.split("/")[0]:
                    temp = i
            if temp != 0:
                return temp.goto("/".join(path.split("/")[1:]))

    def read(self, filename):
        for i in self.lst:
            if isinstance(i, File) and i.name == filename:
                return i.read()

    def __str__(self):
        return self.list()


class File:
    def __init__(self, name):
        temp = name.split('.')
        if len(temp) != 2:
            print("Name of file in bad format")
            raise ValueError
        else:
            self.name = name.split('.')[0]
            self.type = name.split('.')[1]
            self.content = [""]

    def read(self):
        res = ""
        if len(self.content) != 0:
            for i in range(len(self.content)):
                res = res + self.content[i]
                if i != len(self.content) - 1:
                    res = res + "\n"
        return res

    def write(self, string):
        if string != "":
            self.content.append(string)

    def __str__(self):
        return self.read()


a = File("test.txt")
a.write("baby")
a.write("cookie")

b = Directory("baby")
c = Directory("cookie")
print(a)

b.add(a)
b.add(c)
print(b)

d = FileSystem()
d.create_dir("a/b")
d.create_file("a/b", a)
d.create_file("a/b", a)
print(d.list("a/b"))
