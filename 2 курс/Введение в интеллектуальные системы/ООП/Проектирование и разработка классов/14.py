class Server:
    def __init__(self, name):
        self.users = {}
        self.name = name

    def add_user(self, user):
        if user not in self.users.keys():
            self.users[user] = []
            print("Пользователь был успешно добавлен.")
        else:
            print("Такой пользователь уже есть в базе.")

    def get_messages(self, user):
        if user not in self.users.keys():
            print("Такого пользователя нет в базе.")
        elif not self.users[user]:
            print("Почта пуста.")
        else:
            messages = self.users[user]
            self.users[user] = []
            return messages

    def add_message(self, user, msg):
        if user not in self.users.keys():
            print("Такого пользователя нет в базе.")
        else:
            self.users[user].append(msg)
            print("Сообщение успешно отправлено.")


class MailClient:
    servers = []

    def __init__(self, server, user):
        if not isinstance(server, Server):
            print("Это не сервер.")
        elif server not in MailClient.servers:
            print("Данный сервер не поддерживается клиентом.")
        else:
            MailClient.servers[MailClient.servers.index(server)].add_user(user)
            self.user = user
            self.server = server

    def receive_mail(self):
        return MailClient.servers[MailClient.servers.index(self.server)].get_messages(self.user)

    @staticmethod
    def send_mail(server, user, message):
        if not isinstance(server, Server):
            print("Это не сервер.")
        elif server not in MailClient.servers:
            print("Данный сервер не поддерживается клиентом.")
        else:
            MailClient.servers[MailClient.servers.index(server)].add_message(user, message)

    @staticmethod
    def add_server(server):
        if not isinstance(server, Server):
            print("Это не сервер.")
        elif server in MailClient.servers:
            print("Этот сервер уже есть в базе.")
        else:
            MailClient.servers.append(server)
            print("Сервер был успешно добавлен.")

    @staticmethod
    def get_servers():
        return [server.name for server in MailClient.servers]


s1 = Server("Charlie")
s2 = Server("Beta")
s3 = Server("Alpha")

MailClient.add_server(s1)
MailClient.add_server(s2)
MailClient.add_server(s3)
print()

client = MailClient(s1, "Alex")
client.receive_mail()
print()

client1 = MailClient(s2, "User")
client1.receive_mail()
print()

client.send_mail(s2, "User", "Hi!")
print()

print(*client1.receive_mail())
print()

print(*client.get_servers())
