class X:
    @staticmethod
    def call(x):
        return x


class SqrtFun:
    @staticmethod
    def call(x):
        return x ** 0.5


class Const:
    def __init__(self, value):
        self.value = value

    def call(self, x):
        return self.value


class Expression:
    def __init__(self, f1, op, f2):
        self.f1 = f1
        self.op = op
        self.f2 = f2

    def call(self, x):
        first = self.f1.call(x)
        second = self.f2.call(x)
        if self.op == '+':
            return first + second
        if self.op == '-':
            return first - second
        if self.op == '*':
            return first * second
        if self.op == '/':
            return first / second


functions = {'x': X(), 'sqrt_fun': SqrtFun()}

n = int(input("Введите число команд.\n"))
for i in range(n):
    command, *args = input().split()

    if command == "define":
        name, f1, op, f2 = args

        if f1.isdigit() or (f1[0] == '-' and f1[1:].isdigit()):
            f1 = Const(int(f1))
        else:
            f1 = functions[f1]
        if f2.isdigit() or (f2[0] == '-' and f2[1:].isdigit()):
            f2 = Const(int(f2))
        else:
            f2 = functions[f2]

        functions[name] = Expression(f1, op, f2)
    elif command == "calculate":
        name, *x_val = args
        print(' '.join(str(functions[name].call(int(x))) for x in x_val))
