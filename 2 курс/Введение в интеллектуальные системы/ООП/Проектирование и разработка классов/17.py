class Time:
    def __init__(self, time):
        temp = time.split(':')
        if len(temp) == 3 and 0 <= int(temp[0]) < 24 and 0 <= int(temp[1]) < 60 and 0 <= int(temp[2]) < 60 \
                and len(temp[0]) == 2 and len(temp[1]) == 2 and len(temp[2]) == 2:
            self.time = time
        else:
            print("Start time in bad format")
            raise ValueError

    def __str__(self):
        return self.time

    def __lt__(self, other):
        selftime = self.time.split(":")
        othertime = other.time.split(":")
        for i in range(3):
            if int(selftime[i]) < int(othertime[i]):
                return True
            else:
                return False

    def __gt__(self, other):
        selftime = self.time.split(":")
        othertime = other.time.split(":")
        for i in range(3):
            if int(selftime[i]) > int(othertime[i]):
                return True
            else:
                return False


class Presentation:
    def __init__(self, theme, start_time, duration):
        self.theme = theme
        self.start_time = Time(start_time)
        self.duration = duration if 0 < duration < 3 else 0
        if self.duration == 0:
            print("Duration in bad format")
            raise ValueError

    def __repr__(self):
        return self.theme


class Conference:

    def __init__(self, name, start_time):
        self.name = name
        self.start_time = Time(start_time)
        self.presentations = []

    def __str__(self):
        return "Conference", self.name, " starting in ", self.start_time, "\n Presentations: "

    def get_presentations(self):
        return self.presentations if len(self.presentations) != 0 else "There is no added presentations"

    def add(self, presentation):
        if not isinstance(presentation, Presentation) or presentation.start_time < self.start_time:
            print("Presentation in bad format")
            raise ValueError
        if presentation in self.presentations:
            print("This presentation was added already")
        else:
            self.presentations.append(presentation)

    def presentation_time(self, theme):
        for i in self.presentations:
            if i.theme == theme:
                return i.duration
        print("There is no such presentation")


c = Conference("Animals", "10:00:00")
p1 = Presentation("Cats", "10:00:00", 2)
p2 = Presentation("Dogs", "13:00:00", 1)
p3 = Presentation("Penguins", "15:00:00", 1)

print(c.get_presentations())
c.add(p1)
c.add(p2)
c.add(p3)
print(c.get_presentations())
print(c.presentation_time("Cats"))
print(c.presentation_time("Dogs"))
print(c.presentation_time("Penguins"))
