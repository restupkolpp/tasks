class Selector:
    def __init__(self, v):
        self.v = v.copy()

    def get_odds(self):
        return list(filter(lambda x: x % 2 != 0, self.v))

    def get_evens(self):
        return list(filter(lambda x: x % 2 == 0, self.v))


values = [11, 12, 13, 14, 15, 16, 22, 44, 66]
selector = Selector(values)
values.clear()
odds = selector.get_odds()
evens = selector.get_evens()
print(' '.join(map(str, odds)))
print(' '.join(map(str, evens)))
