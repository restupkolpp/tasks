class Balance:
    def __init__(self):
        self.r = 0
        self.l = 0

    def add_right(self, n):
        self.r += n

    def add_left(self, n):
        self.l += n

    def result(self):
        if self.r == self.l:
            return '='
        if self.r < self.l:
            return 'L'
        if self.r > self.l:
            return 'R'


balance = Balance()
balance.add_right(10)
balance.add_left(9)
balance.add_left(2)
print(balance.result())
