import math


class Weapon(object):
    def __init__(self, name, damage, radius):
        self.name = name
        self.damage = damage
        self.radius = radius

    def hit(self, actor, target):
        if not target.is_alive():
            print("Враг уже повержен")
            return
        elif self.radius >= ((target.pos_x - actor.pos_x) ** 2 + (target.pos_y - actor.pos_y) ** 2) ** 0.5:
            target.get_damage(self.damage)
            print("Врагу нанесён урон оружием ", self.name, " в размере ", self.damage)
        else:
            print("Враг слишком далеко для оружия " + self.name)

    def __str__(self):
        return self.name


class BaseCharacter(object):
    def __init__(self, pos_x, pos_y, hp):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.hp = hp

    def move(self, delta_x, delta_y):
        self.pos_x = self.pos_x + delta_x
        self.pos_y = self.pos_y + delta_y

    def is_alive(self):
        if self.hp > 0:
            return True
        else:
            return False

    def get_damage(self, amount):
        self.hp -= amount

    def get_coords(self):
        return [self.pos_x, self.pos_y]


class BaseEnemy(BaseCharacter):
    def __init__(self, pos_x, pos_y, weapon, hp):
        super().__init__(pos_x, pos_y, hp)
        self.weapon = weapon

    def hit(self, target):
        if isinstance(target, MainHero):
            self.weapon.hit(self, target)
        else:
            print("Могу ударить только Главного героя.")

    def __str__(self):
        return "Враг на позиции (" + self.pos_x + " " + self.pos_y + ") с оружием " + self.weapon.name


class MainHero(BaseCharacter):
    def __init__(self, pos_x, pos_y, name, hp):
        super().__init__(pos_x, pos_y, hp)
        self.name = name
        self.weaponlist = []
        self.weapon = None

    def hit(self, target):
        if self.weapon is None:
            print("Я безоружен")
        elif isinstance(target, BaseEnemy):
            self.weapon.hit(self, target)
        else:
            print("Могу ударить только Врага")

    def add_weapon(self, weapon):
        if isinstance(weapon, Weapon):
            self.weaponlist.append(weapon)
            self.weapon = weapon if len(self.weaponlist) == 1 else self.weapon
            print("Подобрал " + weapon.name)
        else:
            print("Это не оружие")

    def next_weapon(self):
        if not len(self.weaponlist):
            print("Я безоружен")
        elif len(self.weaponlist) == 1:
            print("У меня только одно оружие")
        else:
            self.weapon = self.weaponlist[self.weaponlist.index(self.weapon) + 1]
            print("Сменил оружие на " + self.weapon.name)

    def heal(self, amount):
        self.hp += amount if self.hp + amount <= 200 else 200 - self.hp
        print("Полечился, теперь здоровья " + self.hp)


weapon1 = Weapon("Короткий меч", 5, 1)
weapon2 = Weapon("Длинный меч", 7, 2)
weapon3 = Weapon("Лук", 3, 10)
weapon4 = Weapon("Лазерная орбитальная пушка", 1000, 1000)
princess = BaseCharacter(100, 100, 100)
archer = BaseEnemy(50, 50, weapon3, 100)
armored_swordsman = BaseEnemy(10, 10, weapon2, 500)
archer.hit(armored_swordsman)
armored_swordsman.move(10, 10)
print(armored_swordsman.get_coords())
main_hero = MainHero(0, 0, "Король Артур", 200)
main_hero.hit(armored_swordsman)
main_hero.next_weapon()
main_hero.add_weapon(weapon1)
main_hero.hit(armored_swordsman)
main_hero.add_weapon(weapon4)
main_hero.hit(armored_swordsman)
main_hero.next_weapon()
main_hero.hit(princess)
main_hero.hit(armored_swordsman)
main_hero.hit(armored_swordsman)
