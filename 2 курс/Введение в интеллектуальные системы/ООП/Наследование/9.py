class Triangle(object):
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def perimeter(self):
        return self.a + self.b + self.c


class EquilateralTriangle(Triangle):
    def __init__(self, a):
        Triangle.a = a
        Triangle.b = a
        Triangle.c = a
        super().__init__(Triangle.a, Triangle.b, Triangle.c)

    def perimeter(self):
        return Triangle.perimeter()


Triangle = Triangle(3, 7, 10)
print(Triangle.perimeter())
Triangle1 = EquilateralTriangle(6)
print(Triangle1.perimeter())
