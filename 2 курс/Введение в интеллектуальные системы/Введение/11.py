a = int(input())
for i in range(1, a + 1):
    spaces = (a - i) * ' '
    stars = i * '*' + (i - 1) * '*'
    print(f'{spaces}{stars}')
