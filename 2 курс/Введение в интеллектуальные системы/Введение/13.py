h = int(input())
w = int(input())
c = input()
for i in range(h):
    if i == 0 or i == h - 1:
        for j in range(w):
            print(c, end='')
    else:
        print(c, end='')
        for j in range(1, w - 1):
            print(' ', end='')
        print(c, end='')
    print()
