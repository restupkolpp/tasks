matrix = [];
m = 3;
n = 4;

for (let i = 0; i < m; i++) 
{
    matrix[i] = [];

    for (let j = 0; j < n; j++) 
    {
        matrix[i][j] = Math.floor(Math.random() * 20) - 10;
    }   
}

for (let i = 0; i < m; i++) 
{
    console.log(matrix[i]);
}

maxSum = 0;
for (let i = 0; i < m; i++) 
{
    let max = matrix[i][0];

    for (let j = 1; j < n; j++) 
    {
        if (matrix[i][j] > max) 
        {
            max = matrix[i][j];
        }
    }

    maxSum += max;
}
console.log(maxSum);

minProduct = 1;
for (let j = 0; j < n; j++)
{
    let min = matrix[0][j];

    for (let i = 1; i < m; i++) 
    {
        if (matrix[i][j] < min) 
        {
            min = matrix[i][j];
        }
    }

    minProduct *= min;
}
console.log(minProduct);
