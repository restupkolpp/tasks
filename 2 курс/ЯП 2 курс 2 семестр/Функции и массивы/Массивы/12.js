let arr = [1, 2, -3, 4, 5, 6, 7, 8];
for (let i = 0; i < arr.length; i++)
{
    if ((arr[i] > 0) && (i % 2 != 0))
    {
        arr[i] = arr[i] * 3;
    }
    if ((arr[i] < 0) && (i % 2 == 0))
    {
        arr[i] = arr[i] / 5;
    }
}

console.log(arr);