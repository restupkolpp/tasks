function polygonPerimeter(x, y) 
{
    let perimeter = 0;
    const n = x.length;
  
    for (let i = 0; i < n; i++) 
    {
      let dx = x[(i + 1) % n] - x[i];
      let dy = y[(i + 1) % n] - y[i];

      perimeter += Math.sqrt(dx * dx + dy * dy);
    }
    return perimeter;
} 

let x = [];
let y = [];
let n = Math.floor(Math.random() * 5);

for (var i = 0; i < n; i++)
{
    datax = Math.floor(Math.random() * 10);
    x.push(datax);

    datay = Math.floor(Math.random() * 10);
    y.push(datay);
}

console.log(x);
console.log(y);

console.log(polygonPerimeter(x, y));