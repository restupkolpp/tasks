function add() 
{
    let list = document.createElement('ul');
    document.body.append(list);
    
    while (true)
    {
        let value = ''

        while (value == '')
        {
            value = prompt('Enter element value!');
        }
        
        if (value == null)
        {
            break;
        }

        let elem = document.createElement('li');
        elem.innerHTML = value;
        list.appendChild(elem);
    }
}