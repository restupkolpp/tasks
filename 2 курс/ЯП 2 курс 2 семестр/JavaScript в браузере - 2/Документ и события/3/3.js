function getTable()
{
    return document.getElementById("age-table");
}

function getLabels()
{
    return getTable().getElementsByTagName('label');
}

function getTd()
{
    return getTable().getElementsByTagName('td')[0];   
}

function getForm()
{
    return document.getElementsByName('search')[0];
}

function getFirstInput()
{
    return getForm().getElementsByTagName('input')[0];
}

function getLastInput()
{
    return getForm().lastElementChild;
}

console.log(getTable());
console.log(getLabels());
console.log(getTd());
console.log(getForm());
console.log(getFirstInput());
console.log(getLastInput());