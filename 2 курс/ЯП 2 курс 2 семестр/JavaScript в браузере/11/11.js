function rand()
{
    return Math.floor(Math.random() * 10);
}

function func()
{
    let n = Number(document.getElementsByName('text')[0].value)
    let d = []
    let max_min = []
    let arr_before = ''
    let arr_after = ''
    
    for (let i = 0; i < n; i++)
    {
        const row = Array.from(Array(n), rand());
        arr.push(row);
    }

    for (let i = 0; i < arr.length; i++)
    {
        for (let j = 0; j < arr.length; j++)
        {
            arr_before += arr[i][j] + ' ';
        }
        arr_before += '\n';
    }

    for (let i = 0; i < arr.length; i++)
    {
        d.push(arr[i][i], arr[i][arr.length - i - 1]);
    }
    
    max_min.push(Math.max(...d), Math.min(...d));

    for (let i = 0; i < arr.length; i++)
    {
        for (let j = 0; j < arr.length; j++)
        {
            if ( (i == j || j == arr.length - i - 1) && !max_min.includes(arr[i][j]))
            {
                arr[i][j] = 0;
            }
        }
    }

    for (let i = 0; i < arr.length; i++)
    {
        for (let j = 0; j < arr.length; j++)
        {
            arr_after += arr[i][j] + ' ';
        }
        arr_after += '\n';
    }

    alert(arr_before + '\n' + arr_after);
}
