function func()
{
    let res = new Date();

    let date = res.getTime() - new Date(2023, 08, 01).getTime()
    res.setTime(Math.abs(date));

    alert(res.getMonth() + 12 * Math.abs(res.getFullYear() - 1970));
}
