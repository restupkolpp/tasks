async function complete()
{
    await(await import('./Modules/1_readConfig.mjs')).default('myConfig')
    await(await import('./Modules/2_doQuery.mjs')).default('select * from cities');
    await(await import('./Modules/3_httpGet.mjs')).default('http://google.com');
    await(await import('./Modules/4_readFile.mjs')).default('README.md');
}

complete();