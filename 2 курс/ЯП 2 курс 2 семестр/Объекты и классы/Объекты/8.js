function aboutBrowser()
{
    console.log("Имя", Browser.name);
    console.log("Версия", Browser.version);
}

function Browser(name, version)
{
    this.name = name;
    this.version = version;
    this.aboutBrowser = aboutBrowser;
}

let myBrowser = new Browser("Microsoft Internet Explorer", "9.0");

console.log(myBrowser.name);
console.log(myBrowser.version);

myBrowser.aboutBrowser();                                       