class Stock {
    constructor() {
        this.arr = [];
    }

    add(w, v) {
        this.arr.push([w, v]);
        return this;
    }

    getByW(min_w) {
        let index = 0;
        let min = min_w;
        for (let i = 0; i < this.arr.length; i++)
        {
            if (this.arr[i][0] >= min)
            {
                min = this.arr[i][0];
                index = i;
            }
        }
        return index;
    }

    getByV(min_v) {
        let index = 0;
        let min = min_v;
        for (let i = 0; i < this.arr.length; i++)
        {
            if (this.arr[i][1] >= min)
            {
                min = this.arr[i][1];
                index = i;
            }
        }
        return index;
    }
}

let stock = new Stock();
stock.add(10, 20).add(20, 25).add(30, 10).add(35, 10);
console.log(stock.getByW(25));
console.log(stock.getByV(20));