class Clock 
{
    constructor(hours, mins, sec)
    {
        this.hours = hours;
        this.mins = mins;
        this.sec = sec;
    }
    show()
    {
        console.log("h: " + this.hours + " m: " + this.mins + " s: " + this.sec);
    }
}

let clocks = new Clock(5, 23, 45);
clocks.show();