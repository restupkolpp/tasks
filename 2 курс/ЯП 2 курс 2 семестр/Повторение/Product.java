public class Product
{
    public String title;
    public String data;
    public int price;

    public Product(String title, String data, int price)
    {
        this.title = title;
        this.data = data;
        this.price = price;
    }

    public String toString()
    {
        return "Product: " + this.title + " " + this.data + " " + this.price;
    }
}
