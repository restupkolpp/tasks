public class Human
{
    public String surname;
    public String name;
    public String patronymic;
    public String data;
    public String pol;

    public Human(String surname, String name, String patronymic, String data, String pol)
    {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.data = data;
        this.pol = pol;
    }
}
