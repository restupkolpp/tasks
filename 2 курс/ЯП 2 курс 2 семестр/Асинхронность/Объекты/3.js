function F(x)
{
    f1(x);
}

function f1(x)
{
    setTimeout(() => {
        let res = x ** 2;
        console.log('f1: ' + res);
        f1_notify(x, res);
    }, 0);
}
function f1_notify(x, result)
{
    f2(x, result);
}

function f2(x, result)
{
    setTimeout(() => {
        let res = result +  2 * x;
        console.log('f2: ' + res);
        f2_notify(x, res);
    }, 0);
}
function f2_notify(x, result)
{
    f3(x, result);
}

function f3(x, result)
{
    setTimeout(() => {
        let res = result - 2;
        console.log('f3: ' + res);
        f3_notify(x, res);
    }, 0);
}
function f3_notify(x, result)
{
    f4(x, result);
}

function f4(x, result)
{
    setTimeout(() => {
        let res = result +  x;
        console.log('f4: ' + res);
        f4_notify(x, res);
    }, 0);
}
function f4_notify(x, result)
{
    f5(x, result);
}

function f5(x, result)
{
    setTimeout(() => {
        let res = result + x ** 3;
        console.log('f5: ' + res);
        f5_notify(x, res);
    }, 0);
}
function f5_notify(x, result)
{
    f6(x, result);
}

function f6(x, result)
{
    setTimeout(() => {
        let res = result - 12;
        console.log('f6: ' + res);
        f6_notify(res);
    }, 0);
}
function f6_notify(result)
{
    console.log('F: ' + result);
}

F(3);