function F(x)
{
    f1(x).
    then((n) => f2(n[0], n[1])).
    then((n) => f3(n[0], n[1])).
    then((n) => f4(n[0], n[1])).
    then((n) => f5(n[0], n[1])).
    then((n) => f6(n[0], n[1])).
    then((n) => console.log("F:", n[1]));
}

async function f1(x)
{
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let res = x ** 2;
            console.log('f1: ' + res);
            resolve([x, res])
        }, 0);
    });
}

async function f2(x, result)
{
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let res = result +  2 * x;
            console.log('f2: ' + res);
            resolve([x, res])
        }, 0);
    });
}

async function f3(x, result)
{
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let res = result - 2;
            console.log('f3: ' + res);
            resolve([x, res])
        }, 0);
    });
}

async function f4(x, result)
{
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let res = result +  x;
            console.log('f4: ' + res);
            resolve([x, res])
        }, 0);
    });
}

async function f5(x, result)
{
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let res = result + x ** 3;
            console.log('f5: ' + res);
            resolve([x, res])
        }, 0);
    });
}

async function f6(x, result)
{
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let res = result - 12;
            console.log('f6: ' + res);
            resolve([x, res])
        }, 0);
    });
}

F(3);