async function delay()
{
    return new Promise(resolve=>
        setTimeout(resolve, Math.random() * 1000)
    );
}

async function readConfig (name) 
{
    await delay();
    console.log('(1) config from ' + name + ' loaded')
}

async function doQuery (statement) 
{
    await delay();
    console.log('(2) SQL query executed: ' + statement)
}

async function httpGet (url) 
{
    await delay();
    console.log('(3) Page retrieved: ' + url)
}

async function readFile (path) 
{
    await delay();
    console.log('(4) Readme file from ' + path + ' loaded')
}

async function complete()
{
    await readConfig('myConfig');
    await doQuery('select * from cities');
    await httpGet('http://google.com');
    await readFile('README.md');
}

complete();