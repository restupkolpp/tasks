async function func(a, b)
{
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (func.count == 5)
            {
                reject("Ошибка");
            }
            else if (typeof(a) != "number" || typeof(b) != "number")
            {
                reject("Введены не числовые значения");
            }
            else
            {
                console.log("Count:", func.count, "Sum:", a + b)
                func.count++;
                resolve(a + b);
            }    
        }, 2000);
    });
}

func.count = 1;
async function result()
{
    try
    {
        let res1 = await func(1, 2);
        let res2 = await func(res1, 2);
        let res3 = await func(res2, 2);
        let res4 = await func(res3, 2);
        let res5 = await func(res4, 2);
        let res6 = await func(res5, 2);
    }
    catch(e)
    {
        console.log(e);
    }
}

result();
